#include <iomanip>
#include "mcv_platform.h"
#include "comp_camera2D.h"
#include "components/common/comp_transform.h"
#include "utils/utils.h"
#include "engine.h"
#include "input/input_module.h"
#include "entity/entity_parser.h"
#include "sinn/characters/player/comp_player.h"
#include "components/common/comp_camera.h"
#include <iostream>

DECL_OBJ_MANAGER("camera_2D", TCompCamera2D);

float angle = 1;
void TCompCamera2D::debugInMenu() {
    /*ImGui::DragFloat3("Look debug", &debug_look.x, 0.1, -1000, 1000, "%.2f");
    ImGui::DragFloat3("CamPos debug", &debug_camPos.x, 0.1, -1000, 1000, "%.2f");*/

    ImGui::LabelText("Dist Shadow To Camera", "%f", dis_shadow_to_camera);
    ImGui::DragFloat("Offset Shadow To Camera", &offset_shadow_to_camera);
    ImGui::DragFloat("Min distance Shadow To Camera", &min_dis_shadow_to_camera);

    ImGui::DragFloat3("Position", &debug_cam_pos.x);
    ImGui::DragFloat3("Look", &debug_cam_look.x);
    ImGui::LabelText("Movimiento Raton", "%f %f", mOff.x, mOff.y);
    ImGui::DragFloat("Horizontal Offset", &hOffset2D);
    ImGui::DragFloat("Vertical Offset", &vOffset2D);
    if (ImGui::SmallButton("Reiniciar Offsets")) {
        resetcam2D();
    }
    ImGui::DragFloat("Max Horizontal Offset", &max_hOffset2D);
    ImGui::DragFloat("Max Vertical Offset", &max_vOffset2D);
    ImGui::Checkbox("Invertir Movimiento Cam2D", &paneoInverso);
    ImGui::Checkbox("Vuelta poco a poco", &vueltaPorVelocidad);
    ImGui::LabelText("Delay timer", "%.1f", timerDelayReturn);
    ImGui::ProgressBar(timerDelayReturn / delayReturn, ImVec2(-1, 0), "Delay");
    ImGui::LabelText("Volviendo...", useVelToReduce ? "True" : "False");

  
    ImGui::DragFloat("Sensitivity", &_sensitivity, 0.001f, 0.001f, 10);
    ImGui::LabelText("Distance Center Line", "%f", distance_center_line);
    ImGui::DragFloat("Pitch raton", &_pitchRatio);
    ImGui::DragFloat("Yaw", &_yaw);
    ImGui::LabelText("Curve", "%s", curveFilename.c_str());
    curve->renderInMenu();
}

void TCompCamera2D::load(const json& j, TEntityParseContext& ctx) {
    _sensitivity = j.value("sensitivity", _sensitivity);
    curveFilename = j["curve"];
    curve = Resources.get(curveFilename)->as<CCurve>();
    _pitchRatio = j.value("pitchRatio", _pitchRatio);
    _padSensitivity = j.value("padsensitivity", _padSensitivity);
    velReduceOffset = j.value("offset_vel", velReduceOffset);

    if (j.contains("maxH_formValues")) {
        hgv = loadFloatVecN(j, "maxH_formValues", 10);
    }

    if (j.contains("maxV_formValues")) {
        vgv = loadFloatVecN(j, "maxV_formValues", 5);
    }

}

//the camera will focus on the shadow of the player but with the body of the player in sight
void TCompCamera2D::renderCam2D(VEC3 wall_normal, VEC3 wall_point)
{
    TCompPlayer* player = hplayer->get<TCompPlayer>();
    TCompTransform* c_transform = get<TCompTransform>();
    TCompTransform* htrans = hplayer->get<TCompTransform>();
    TCompCamera* myCam = get<TCompCamera>();

    if (!c_transform) return;
    VEC3 oldPos = c_transform->getPosition();

    VEC3 newPos, look;

    //am I going left to right
            //we calculate the distance from the shadow to the body
    distance_shadow_body = VEC3::Distance(htrans->getPosition(), wall_point);
    distance_shadow_body = float_one_point_round(distance_shadow_body);
    
    /* if (distance_shadow_body > 30)
       distance_shadow_body *=0.8;*/
       //we try to get a line that goes from the body or shadow to wherever we want the camera to go
    s = (distance_shadow_body / 2) / sen20;
    //with that s, we get the line that goes from the center point of shadow and body to S in what should be a circle that englobes shadow, body and camera
    distance_center_line = s * cos20;
    distance_center_line = float_one_point_round(distance_center_line);
    //to make sure the camera doesn't go behind the wall we check both possible camera points and the closest to a point outside the wall will be the chosen one
    VEC3 wall_normal_2;
    wall_normal_2 = wall_normal * 2; //a point outside the wall, using its normal
    float radius = s * sen20 / sen140; //this will be the radius of the circle that englobes shadow, player and camera
    //we get the centre point between shadow and body

    float distance_centre_pointX = (htrans->getPosition().x + wall_point.x) / 2;
    float distance_centre_pointZ = (htrans->getPosition().z + wall_point.z) / 2;
    //and the perpendicular vector of the line that goes from shadow to body
    float perpendicular_distanceX = -(wall_point.z - htrans->getPosition().z) / distance_shadow_body;
    float perpendicular_distanceZ = (wall_point.x - htrans->getPosition().x) / distance_shadow_body;
    //we try to get the centre of the circle that englobes shadow, body and camera, there are two possibilities
    VEC3 centrePoint = VEC3(distance_centre_pointX, htrans->getPosition().y + 1, distance_centre_pointZ) + VEC3(perpendicular_distanceX, htrans->getPosition().y + 1, perpendicular_distanceZ) * (distance_center_line - radius);
    VEC3 centrePoint2 = VEC3(distance_centre_pointX, htrans->getPosition().y + 1, distance_centre_pointZ) - VEC3(perpendicular_distanceX, htrans->getPosition().y + 1, perpendicular_distanceZ) * (distance_center_line - radius);
    //calculate each possibility with that normalPoint
    float dis_centre1_normal = VEC3::Distance(centrePoint, wall_normal_2);
    float dis_centre2_normal = VEC3::Distance(centrePoint2, wall_normal_2);

    VEC3 myCentrePoint;
    //the closest one will be the camera that is in front of the wall
    if (dis_centre1_normal < dis_centre2_normal) {
        myCentrePoint = centrePoint;
    }
    else {
        myCentrePoint = centrePoint2;
    }
    //now we need the angle between the vectors that go shadow to centre and the normal of the wall
    VEC3 shadow_to_centre = (myCentrePoint - wall_point) / radius; //calculate line that goes shadow to centre
    VEC3 unit_wall_normal = wall_normal / wall_normal.Length();//get the normal in units of 1 and 0 (unitize)
    //to get the angle between two vectors we need to do the acosen of the dot product of such vectors
    float dotProduct = unit_wall_normal.Dot(shadow_to_centre);
   
    // Change the distance shadow to camera for absolute value -> dis_shadow_to_camera = distance_shadow_body * 1.3;
    dis_shadow_to_camera = distance_shadow_body + offset_shadow_to_camera;
    if (dis_shadow_to_camera < min_dis_shadow_to_camera) dis_shadow_to_camera = min_dis_shadow_to_camera;

    float dif_in_y = htrans->getPosition().y - wall_point.y + 1;
    //the position of the camera will now be the distance calculated multiplied by the direction of the wall normal and the position of the shadow  

    newPos = debug_camPos = (unit_wall_normal * dis_shadow_to_camera) + wall_point + VEC3(0, dif_in_y, 0);
    look = debug_look = wall_point;

    VEC3 wallRight = VEC3::Up.Cross(wall_normal);

    calculateMovementOffset(&newPos, &look, wallRight);

    debug_cam_pos = newPos;
    debug_cam_look = look;

    
    if (without_init) {
        without_init = false;
        camPos = newPos;
        newLook = look;
    }
    else {
        camPos = 0.9f * oldPos + 0.1f * newPos;
        newLook = 0.9f * oldLook + 0.1f * look;
    }

}


