#include "mcv_platform.h"
#include "comp_frames.h"
#include "components/common/comp_collider.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_camera.h"
#include "components/common/comp_render.h"
#include "engine.h"
#include "modules/module_camera_mixer.h"

DECL_OBJ_MANAGER("frames", TCompFrames);



void TCompFrames::renderDebug()
{
}

void TCompFrames::debugInMenu()
{
	if (ImGui::Button("activate")) {
		activate();
	}
}

void TCompFrames::load(const json& j, TEntityParseContext& ctx)
{
	frame_rate = j.value("frame_rate", frame_rate);
	for (auto e : j["keys"]) {
		keyFrame k;
		k.position = loadVEC3(e,"position");
		k.rotation = loadVEC4(e,"rotation");
		k.lookat = loadVEC3(e, "lookat");
		keys.push_back(k);
	}

}

void TCompFrames::onEntityCreated()
{
}

void TCompFrames::update(float dt)
{
	if (first) {
		initialize();
		first = false;
	}
	if (!activated)return;
	totalTime = keys.size() / frame_rate;
	time += dt;
	int step = (int)(time * frame_rate);
	
	indice += step;
	time -= step / frame_rate;
	//float timestamp = indice / frame_rate;
	if (indice<keys.size()) {

		if (step > 0) {
			
			
			CTransform new_trans;
			new_trans.setPosition(keys.at(indice).position);
			new_trans.setRotation(keys.at(indice).rotation);
			
			TCompTransform* trans = get<TCompTransform>();
			trans->set(new_trans);
			TCompCamera* cam = get<TCompCamera>();
			if (cam) {
				trans->lookAt(trans->getPosition(),keys.at(indice).lookat);
			}
			
		}


	}
	else {
		
		TCompCamera* cam = get<TCompCamera>();
		if (cam) {
			
			CEngine::get().getCameraMixer().blendCamera("cameraWalk3D", 1, &interpolators::linearInterpolator);
		}
		finished = true;
		TCompRender* rend = get<TCompRender>();
		if (rend)rend->activateState(2);
		/*TCompCollider* collider = get<TCompCollider>();
		if (collider) {
			collider->

		}*/
	}
	
}

void TCompFrames::activate()
{
	activated = true;
	indice = 0;
	TCompCamera* cam = get<TCompCamera>();
	if (cam) {
		CEntity* e = CHandle(this).getOwner();
		std::string name = e->getName();
		CEngine::get().getCameraMixer().blendCamera(name, 1, &interpolators::linearInterpolator);
	}
	TCompRender* rend = get<TCompRender>();
	if (rend)rend->activateState(0);
	finished = false;
}

void TCompFrames::initialize()
{
	TCompRender* rend = get<TCompRender>();
	if(rend)rend->activateState(2);
}

void TCompFrames::registerMsgs()
{
}
