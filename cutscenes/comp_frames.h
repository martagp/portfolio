#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
struct keyFrame
{
	VEC3 position;
	QUAT rotation;
	VEC3 lookat;

};
class TCompFrames : public TCompBase
{
	DECL_SIBLING_ACCESS();
	float time = 0;
	float frame_rate = 30;
	int frame = 0;
	float totalTime = 0;
	int indice = 0;
	bool activated = false;
	bool first = true;
	std::vector<keyFrame> keys;
	bool finished = true;
public:
	bool getFinished() { return finished; };
	void renderDebug();
	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	void onEntityCreated();
	void update(float dt);
	void activate();
	void initialize();
	static void registerMsgs();
};