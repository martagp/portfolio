clearListener()
struct CutSceneExporter (
	
	fs = TJsonFormatter(), 
	project_path = mcv_root_path + "bin/",
	base_path = "data/",
	current_scene_name,
	scenes_path = base_path + "scenes/",
	file_dir_name = getFilenameFile  maxfilename ,
	mesh_path = base_path + "meshes/" + file_dir_name + "/" ,
    mats_path = base_path + "materials/" + file_dir_name + "/" ,
	fn exportCompName obj = (
		fs.writeKeyValue "name" obj.name
	),
	
	fn isValidName aname = (
		return findString aname " " == undefined 
	),
		
	fn exportMatrix keyName trans = (
		
		fs.writeKey keyName
		
		local max2mcv = rotateXMatrix -90
		local mcv2max = rotateXMatrix 90
		local mcv_position = trans.position * max2mcv
		
		-- From mcv, we will go to max, apply the max transform and go back to mcv coord system
		local mcv_transform = mcv2max * trans * max2mcv

		-- Take just the rotation as quaterion
		local mcv_quat = mcv_transform.rotationPart as quat
		
		--local mcv_scale = mcv_transform.scalePart
		
		fs.beginObj()
			fs.writeKeyValue "pos" mcv_position
			fs.writeComma()
			fs.writeKeyValue "rotation" mcv_quat
			--fs.writeComma()
			--fs.writeKeyValue "scale" mcv_scale
		fs.endObj()
	),
		
	fn exportCompTransform obj = (
		fs.writeComma()
		exportMatrix "transform" obj.transform
	),
		fn exportCamera obj = (
		fs.beginObj()
		fs.writeKey "entity"
		fs.beginObj()
			exportCompName obj
			fs.writeComma()
			fs.writeKey "transform"
			local max2mcv = rotateXMatrix -90
			local mcv2max = rotateXMatrix 90
			local mcv_position =  obj.position * max2mcv
		
			-- From mcv, we will go to max, apply the max transform and go back to mcv coord system
			local mcv_lookat =  obj.target.position * max2mcv
			local far = abs(distance mcv_lookat mcv_position)

		
			fs.beginObj()
				fs.writeKeyValue "pos" mcv_position
				fs.writeComma()
				fs.writeKeyValue "lookat" mcv_lookat
			fs.endObj()
			fs.writeComma()
			fs.writeKey "camera"			
			fs.beginObj()
				fs.writeKeyValue "fov" obj.fov
				fs.writeComma()
				fs.writeKeyValue "far" far 
			fs.endObj()
		    exportColliders obj [1,1,1]
		fs.endObj()
		fs.endObj()
	),
	fn exportCompRender given_obj = (
		
		-- Check if we can convert the obj to editableMesh, otherwise do not export compRender
		if not canConvertTo given_obj Editable_mesh then return undefined
		
		local obj = copy given_obj
		obj.name = given_obj.name			-- Keep original name
		convertToMesh obj
		
		fs.writeComma()
		fs.writeKey "render" 
		fs.beginArray()
		-- Export each material 
	
		local mesh_name = mesh_path + obj.name + ".mesh"
		local obj_material = obj.material 
		local material_is_multimaterial = (classof obj_material == Multimaterial)
		
		local materials_used = getMaterialsUsedByMesh obj
		
		
		local mat_mcv = 0
		local mat_max_idx = 1
		for mat_used in materials_used do (
			local faces = materials_used[ mat_max_idx ]
			
			-- Get the submaterial confirming we have a multimaterial type
			local mat = obj_material
			if material_is_multimaterial then mat = obj_material[mat_max_idx]
			
			mat_max_idx = mat_max_idx + 1
			if faces == undefined then continue
			if mat != undefined and classof mat != Standardmaterial then throw ("Error exporting mesh " + obj.name + "\nWe can only export Standardmaterial. Found !" + ((classof mat) as string))
			
			local mat_name
			if mat == undefined then (
				default_material_path = base_path + "materials/"	
				mat_name = default_material_path + "missing.material"
			) else (
				mat_name= mats_path + mat.name + ".material"
			)
			
			if mat_mcv > 0 then fs.writeComma()
			
			fs.beginObj()
				fs.writeKeyValue "mesh"mesh_name
				fs.writeComma()
				fs.writeKeyValue "material"mat_name
				fs.writeComma()
				fs.writeKeyValue "subgroup" mat_mcv
			fs.endObj()
			
			mat_mcv = mat_mcv + 1
		)
		
		fs.endArray()
		local obj_path=project_path+mesh_path+obj.name+".mesh"
		-- Do the real export of the geometry
		local caabb = exportMesh obj undefined obj_path
		
		local me = TMaterialExporter project_path:project_path base_path:base_path
		me.exportMaterial obj.mat mats_path obj
		
		delete obj
		return caabb
	),
	fn exportAnim obj = (
		
		local max2mcv = rotateXMatrix -90
		local mcv2max = rotateXMatrix 90
		
		local t0 = animationRange.start
		local t1 = animationRange.end
		local t=t0
		fs.writeComma()
		fs.writekey "frames"
		fs.beginObj()
			
			fs.writeKeyValue "frame_rate" framerate
			fs.writeComma()
			fs.writeKey "keys" 
			fs.beginArray()
			for t=t0 to t1 do(
				if t>t0 then fs.writeComma()
				at time t(
		
					local mcv_position = obj.transform.position * max2mcv
					
					-- From mcv, we will go to max, apply the max transform and go back to mcv coord system
					local mcv_transform = mcv2max * obj.transform * max2mcv

					-- Take just the rotation as quaterion
					local mcv_quat = mcv_transform.rotationPart as quat
					
						
					fs.beginObj()
						
						fs.writeKeyValue "position" mcv_position
						fs.writeComma()
						fs.writeKeyValue "rotation" mcv_quat
						if classof obj == Targetcamera then (
							fs.writeComma()
							local mcv_lookat =  obj.target.position * max2mcv
						
							fs.writeKeyValue "lookat" mcv_lookat
						)						
						
					fs.endObj()
					
				)
			)
			fs.endArray()

		fs.endObj()
	),
	fn exportMeshObj obj = (
		
		fs.beginObj()
		fs.writeKey "entity"
		fs.beginObj()
			
			exportCompName obj
			exportCompTransform obj
			exportCompRender obj
			exportAnim obj
			
		fs.endObj()
		fs.endObj()
	),

		fn exportCamera obj = (
		fs.beginObj()
		fs.writeKey "entity"
		fs.beginObj()
			exportCompName obj
			fs.writeComma()
			fs.writeKey "transform"
			local max2mcv = rotateXMatrix -90
			local mcv2max = rotateXMatrix 90
			local mcv_position =  obj.position * max2mcv
		
			-- From mcv, we will go to max, apply the max transform and go back to mcv coord system
			local mcv_lookat =  obj.target.position * max2mcv
			local far = abs(distance mcv_lookat mcv_position)

		
			fs.beginObj()
				fs.writeKeyValue "pos" mcv_position
				fs.writeComma()
				fs.writeKeyValue "lookat" mcv_lookat
			fs.endObj()
			fs.writeComma()
			fs.writeKey "camera"			
			fs.beginObj()
				fs.writeKeyValue "fov" obj.fov
				fs.writeComma()
				fs.writeKeyValue "far" far 
			fs.endObj()
			exportAnim obj
			
		fs.endObj()
		fs.endObj()
	),



fn exportCutscene = (
	current_scene_name = getFilenameFile  maxfilename
		if current_scene_name == "" then throw "Save the scene with a name before exporting it"
			filePath = project_path+base_path+scenes_path+(getFilenameFile  maxfilename)+".json"
			file = getSaveFileName caption:"Exportar objeto animado" filename:filePath types:"JSON(*.json)|*.json|All|*.*|"

			
			dir_mesh = project_path + mesh_path
			dir_mat = project_path + mats_path
			format "Creating mesh path file %\n" dir_mesh
			format "Creating mat path file %\n" dir_mat
			makedir dir_mesh
			makedir dir_mat
		
		
		
		fs.begin file
		fs.beginArray()
		local n = 0
		for obj in $* do(
			if classof obj == Targetobject then continue 
			if n > 0 then fs.writeComma()
			if classof obj == Targetcamera then exportCamera obj 
			else exportMeshObj obj
			n = n +1 
		)
			fs.endArray()
		fs.end()
	
)
)

--exporter = CutSceneExporter()
--exporter.exportCutscene()	
--exporter.fs.end()