#include "mcv_platform.h"
#include "module_cutscene.h"
#include "engine.h"
#include "components/common/comp_render.h"
#include "sinn/components/comp_frames.h"
#include "sinn/modules/module_scripting.h"
#include "sinn/modules/module_sound.h"
#include "ui/ui_module.h"
#include <windows\application.h>
#include "sinn/characters/animator.h"
#include "render/render_module.h"
#include "ui/ui_widget.h"

void CModuleCutscene::start()
{
  std::string boot_name("module_cutscene");
  TFileContext fc(boot_name);
  json son = loadJson("data/cutscenes.json");
  auto prefabs = son["scenes_to_load"].get< std::vector< std::string > >();
  for (auto& p : prefabs)
    loadScene(p);
  //load the additional data needed for each scene
  if (son.count("scenes_data")) {
    json sData = son["scenes_data"];
    for (const json& jdata : sData) {
      cutSceneData d;
      string name = jdata.value("name", "");
      d.music_audio = jdata.value("music", "");
      d.voice_audio= jdata.value("voice_audio", "");
      d.music_delay = jdata.value("delay_music", 0.f);
      d.voice_delay = jdata.value("delay_voice", 0.f);
      d.hide_seele = jdata.value("hide_seele", false);
      d.anim_seele = jdata.value("anim_seele", "");
      d.anim_pos = loadVEC3(jdata,"anim_pos");
      d.pos_final = loadVEC3(jdata, "pos_final");
      d.fade_in = jdata.value("fade_in", false);
      d.fade_widget = jdata.value("fade_widget", "");
      d.fadeStart = jdata.value("fade_start", 0.0);
      d.fadeDuration = jdata.value("fade_duration", 0.0);
      cutScenes.emplace(name,d);
    }
  }
  //keep a fake version of Seele in case they are animated in the cutscene and prevent them from rendering
  fakeSeele = getEntityByName("fakeSeele");
  CEntity* e = fakeSeele;
  TCompRender* r = e->get<TCompRender>();
  r->activateState(10);
}

void CModuleCutscene::loadScene(const std::string& p) {

  TEntityParseContext ctx;
  PROFILE_FUNCTION_COPY_TEXT(p.c_str());
  dbg("Parsing %s\n", p.c_str());
  parseScene(p, ctx);
  std::string name = ctx.filename;
  name.erase(0,15);
  name.erase(name.size()-5, name.size());
   ctxs.emplace(name,ctx);
}


void CModuleCutscene::update(float elapsed)
{
  PROFILE_FUNCTION("cutscenes");
  if (done) {
    if (!first) {
      returntoGamePlay();
    }
    id = "";
    time = 0;
    first = true;
  }
  else {
    time += elapsed;
    if (first) {
      prepareCutscene();
    }

  }

  
}

void CModuleCutscene::renderDebug()
{
  if (CApplication::get().ImGui) {
    if (ImGui::TreeNode("Cutscenes")) {
      string n = id;
      if (n == "")n = "none";
      ImGui::Text("Current cutscene: %s", n);
      ImGui::Text("time : %f", time*1000);
      ImGui::Text("%s", done ? "true" : "false ");
      for (auto e : ctxs) {
        if (e.first != "seele_anim") {
          if (ImGui::Button(e.first.c_str())) {
            startCutscene(e.first);
            
          }
        }
        
      }
      ImGui::TreePop();
    }
  }
}

void CModuleCutscene::startCutscene(string name)
{
  cutSceneData d = cutScenes.at(name);
  if (d.fade_in) {
    CEngine::get().getUI().activateWidget(d.fade_widget);
    CEngine::get().getUI().getWidgetById(d.fade_widget)->makeChildsFadeOut(d.fadeDuration,d.fadeStart , false);
  }

  int i = 0;
  for (auto e : ctxs) {
    if (e.first.compare(name) == 0) {
      id = e.first;
      for (auto n : e.second.entities_loaded) {
        CEntity* entity = n;
             
        TCompFrames* frames = entity->get<TCompFrames>();
        if (frames)frames->activate();

        current_cutscene = name;
      }
    }
    ++i;
  }
}

void CModuleCutscene::prepareCutscene()
{
  cutSceneData data = cutScenes.at(id);

  //if has animation reproduce it
  if (data.anim_seele != "") {
    CEntity* e = fakeSeele;
    //update position at start of animation
    TCompTransform* t = e->get<TCompTransform>();
    t->setPosition(data.anim_pos);
    //render fake Seele
    TCompRender* r = e->get<TCompRender>();
    r->activateState(0);
    //animate cutscene animation
    TCompAnimator* anim = e->get<TCompAnimator>();
    anim->removeAllCycles();
    anim->removeCurrentAction();
    anim->playActionAnim(data.anim_seele, false, false, false);
  }
  //send event to LUA so that voice and music will be played if there is any
  string e_name;
  e_name.push_back('"');
  e_name.append(data.voice_audio);
  e_name.push_back('"');
  e_name.push_back(',');
  e_name.push_back('"');
  e_name.append(to_string(data.voice_delay));
  e_name.push_back('"');
  e_name.push_back(',');
  e_name.push_back('"');
  e_name.append(data.music_audio);
  e_name.push_back('"');
  e_name.push_back(',');
  e_name.push_back('"');
  e_name.append(to_string(data.music_delay));
  e_name.push_back('"');
  std::string event_name = e_name;
  CEngine::get().getScripting().ThrowEvent(CModuleScripting::Event::CUTSCENE, event_name, 0);
  first = false;
  //if the cutscene requires a fake main character, the real one will not be rendered
  if (data.hide_seele) {

    CEntity* player = getEntityByName("Player");
    TCompRender* rend = player->get<TCompRender>();
    rend->activateState(10);
  }

  //take away Player control
  CEngine::get().getModules().changeToGamestate("GScutscene");
}

void CModuleCutscene::returntoGamePlay()
{
  CEntity* player = getEntityByName("Player");
  cutSceneData data = cutScenes.at(id);
  //if main character was hidden, unhide them
  if (data.hide_seele) {
    TCompRender* rend = player->get<TCompRender>();
    rend->activateState(0);
    //if fake Seele moves, once we get control back Seele should be where fake was 
    std::stringstream coords;
    coords << data.pos_final.x << ',' << data.pos_final.y << ',' << data.pos_final.z;
    CEngine::get().getScripting().ThrowEvent(CModuleScripting::Event::TELEPORT_PLAYER, coords.str(), 0);
  }
  //stop render of fake Seele if used
  if (data.anim_seele != "") {
    CEntity* e = fakeSeele;
    TCompRender* r = e->get<TCompRender>();
    r->activateState(10);
  }
  //return to main music
  CEngine::get().getSound().playBSO("Temple");

  //return control back to Player
  CEngine::get().getModules().changeToGamestate("gameplay");
}

