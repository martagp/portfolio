#pragma once

#include "modules/module.h"
#include "entity/entity.h"
#include "sound/soundEvent.h"
#include "entity/entity_parser.h"

using namespace std;


class CModuleCutscene : public CModule {
protected:
  struct cutSceneData {
    string voice_audio;
    string music_audio;
    float voice_delay;
    float music_delay;
    bool hide_seele;
    string anim_seele;
    VEC3 anim_pos;
    VEC3 pos_final;
    bool fade_in;
    string fade_widget;
    float fadeStart;
    float fadeDuration;
  };
private:
  std::unordered_map <std::string,cutSceneData> cutScenes;
  std::unordered_map <std::string,TEntityParseContext > ctxs;
  std::string current_cutscene="";
  string id = "";
  float time = 0;
  bool first = true;
  CHandle fakeSeele;
  bool done = true;
public:

  void setFinished(bool f) { done = f; };
  CModuleCutscene(const std::string& name) : CModule(name) {}
  void start() override;
  void loadScene(const std::string& p);
  bool getFinished() { return done; };
  void stop() override {};
  void update(float elapsed) override;
  void renderDebug();

  void startCutscene(string name);
  void prepareCutscene();
  void returntoGamePlay();
};

