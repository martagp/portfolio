#include "mcv_platform.h"
#include "comp_player.h"
#include "components/common/comp_render.h"
#include "components/common/comp_camera.h"
#include "components/common/comp_transform.h"
#include "modules/module_camera_mixer.h"
#include "components/common/comp_aabb.h"
#include "windows/application.h"
#include "sinn/cameras/comp_camera3D.h"
#include "sinn/cameras/comp_camera2D.h"
#include "sinn/components/comp_cuerda.h"
#include <skeleton\comp_skeleton.h>
#include <sinn\behavior\ai\ai_controller.h>
#include "render/render_module.h"
#include "sinn/modules/module_scripting.h"
#include "sinn/modules/module_sound.h"
#include "sinn/components/comp_audio.h"

DECL_OBJ_MANAGER("player", TCompPlayer);

void TCompPlayer::registerMsgs() {
	DECL_MSG(TCompPlayer, TMsgPush, hitBox);
	DECL_MSG(TCompPlayer, TMsgCheckpoint, saveGame);
	DECL_MSG(TCompPlayer, TMsgConstrict, onConstrict);
	DECL_MSG(TCompPlayer, TMsgDeath, onDead);
	DECL_MSG(TCompPlayer, TMsgDeathFall, onFallingDead);
	DECL_MSG(TCompPlayer, TMsgRecordPath, hitWithActor);
	DECL_MSG(TCompPlayer, TMsgPlatMove, OnPlatform);
}

void TCompPlayer::activateSteps(float distanceLeft, float distanceRight, bool n)
{
	stepDistanceLeft = distanceLeft;
	stepDistanceRight = distanceRight;
	stepsActivated = n;
	leftFootInFloor = false;
	rightFootInFloor = false;	
}



void TCompPlayer::playSteps()
{
	if (!stepsActivated) return;


	/*if (VEC3::Distance(t_player->getPosition(), playerStepPos) > stepDistance) {
		playerStepPos = t_player->getPosition();
		TCompAudio* audio = get<TCompAudio>();
		audio->playEvent("Test1/OnStep");
	
	}*/

	if (collisions.collision_down) {
		TCompTransform* t_player = get<TCompTransform>();
		TCompSkeleton* skel = get<TCompSkeleton>();
		
		// Si acabamos de empezar andar y aun no hemos pisado con ninguno de los dos pies
		if (!leftFootInFloor && !rightFootInFloor) {
			if (skel->hasRightFootInFloor(t_player->getPosition().y, stepDistanceRight)) {
				TCompAudio* audio = get<TCompAudio>();
				//dbg("pie derecho Idle\n");
				steps_event = audio->playEvent("Test1/OnStep");
				if (plant.isValid()) {
					TMsgStepPlant msg;
					CEntity* e = plant;
					e->sendMsg(msg);
				}
				rightFootInFloor = true;
			}
			else if (skel->hasLeftFootInFloor(t_player->getPosition().y, stepDistanceLeft)) {
				TCompAudio* audio = get<TCompAudio>();
				//dbg("pie izquierdo Idle\n");
				steps_event = audio->playEvent("Test1/OnStep");
				if (plant.isValid()) {
					TMsgStepPlant msg;
					CEntity* e = plant;
					e->sendMsg(msg);
				}
				leftFootInFloor = true;
			}
		// Si ya hemos pisado con el pie derecho pero aun no con el izquierdo
		}
		else if (!leftFootInFloor && rightFootInFloor) {
			if (skel->hasLeftFootInFloor(t_player->getPosition().y, stepDistanceLeft)) {
				TCompAudio* audio = get<TCompAudio>();
				//dbg("pie izquierdo \n");
				steps_event = audio->playEvent("Test1/OnStep");
				if (plant.isValid()) {
					TMsgStepPlant msg;
					CEntity* e = plant;
					e->sendMsg(msg);
				}
				leftFootInFloor = true;
				rightFootInFloor = false;
			}
		}
		// Si ya hemos pisado con el pie izquierdo pero aun no con el derecho
		else if (leftFootInFloor && !rightFootInFloor) {
			if (skel->hasRightFootInFloor(t_player->getPosition().y, stepDistanceRight)) {
				TCompAudio* audio = get<TCompAudio>();
				//dbg("pie derecho \n");
				steps_event = audio->playEvent("Test1/OnStep");
				if (plant.isValid()) {
					TMsgStepPlant msg;
					CEntity* e = plant;
					e->sendMsg(msg);
				}
				rightFootInFloor = true;
				leftFootInFloor = false;
			}
		}
	}

}


void TCompPlayer::hitBox(const TMsgPush& msg_push) {
	push_box = msg_push.object;
}
SoundEvent* move_box_sound = nullptr;
void TCompPlayer::convertPushBox(bool create_controller) {
	CEntity* ent_box = push_box;
	if (ent_box == nullptr)	return;

	TCompCollider* box_col = ent_box->get<TCompCollider>();
	if (move_box_sound) {
		move_box_sound->stop();
		move_box_sound = nullptr;
	}
	if (create_controller && !box_col->boxcontroller) {
		box_col->destroyActor();
		box_col->recreateController();
	}
	else if(!create_controller){
	
		box_col->destroyBoxController();
		box_col->recreateActorBox();
		push_box = CHandle();
	}
}

void TCompPlayer::movePushBox(float dt) {
	CEntity* ent_box = push_box;
	//assert(ent_box);
	if (ent_box == nullptr) return;
	TCompTransform* t_box = ent_box->get<TCompTransform>();
	VEC3 pos = t_box->getPosition();
	TCompCollider* box_col = ent_box->get<TCompCollider>();
	//assert(box_col->boxcontroller);
	if (box_col->boxcontroller == nullptr) return;
	float dif = VEC3::Distance(pos, oldPos);
	CEngine::get().getPhysics().move(velocity, dt, box_col->boxcontroller, box_collisions);
	oldPos = pos;
	if (velocity.x != 0 && velocity.z != 0 && dif>0.01) {
		TCompAudio* audio = get<TCompAudio>();
		if (move_box_sound)
		{
			if (move_box_sound->isPlaying()) return;
			move_box_sound = audio->playEvent("moveObject");
		}
		else {
			move_box_sound = audio->playEvent("moveObject");
		}

	}
	else {
		if (!move_box_sound)return;
		move_box_sound->stop();

	}
	
}

bool TCompPlayer::canMoveTheBox(){
	PxRaycastBuffer hitCall;
	PxQueryFilterData fd = PxQueryFilterData();
	fd.data.word0 = CModulePhysicsSinn::FilterGroup::Projectable_Object;

	TCompTransform* player_trans = get<TCompTransform>();
	VEC3 player_front = player_trans->getFront(); // Deberia estar ya normalizado
	//player_front.Normalize(); // Deberiamos poderlo quitar
	VEC3 origin = player_trans->getPosition();
	origin.y += 0.5;
	
	bool status = CEngine::get().getPhysics().gScene->raycast(
		VEC3_TO_PXVEC3(origin), VEC3_TO_PXVEC3(player_front),
		4, hitCall, PxHitFlags(PxHitFlag::eDEFAULT), fd);
	if (!status) return status;
	if (!hitCall.hasAnyHits()) {
		if (move_box_sound) {
			move_box_sound->stop();
			move_box_sound = nullptr;
		}
			
		return false;
	}
	CHandle h_collider;
	h_collider.fromVoidPtr(hitCall.block.actor->userData);
	TCompCollider* c_collider = h_collider;
	const json& jconfig = c_collider->jconfig;

	if (jconfig.count("controllerBox") > 0) {
		TCompTransform* box_trans = c_collider->get<TCompTransform>();
		VEC3 half_size = VEC3::Zero;
		if (jconfig.count("shapes")) {
			const json& jshapes = jconfig["shapes"];
			for (const json& jshape : jshapes) {
				std::string jgeometryType = jshape["shape"];
				if (jgeometryType == "box") {
					VEC3 aux = loadVEC3(jshape, "half_size");
					if (aux.x + aux.y + aux.z > half_size.x + half_size.y + half_size.z)
						half_size = aux;
				}
			}			
		}
		if ((1.4f / (half_size.y * 2)) > 1) {
			return true;
		}
	}

}

VEC3 CStatePlayer::getLocalDir3D2D(CContext& ctx) const  // the camera is slightly to the right of the player, so in order to get set the player movement direction, I corrected the angle of the player (since the direction is set by the camera) so they'll move straight
  {
      // We need the horitzontal front of the camera
      CEntity* ent_player = ctx.getOwner();
      TCompPlayer* comp_player = ent_player->get<TCompPlayer>();
      TCompTransform* player_trans = ent_player->get<TCompTransform>();
nte de direccion
      comp_direc->setCurve(camera2D->getCurve());

      float front_length_camera = sqrt(comp_camera->getFront().x * comp_camera->getFront().x + comp_camera->getFront().z * comp_camera->getFront().z);
      float angle_camera_front = acos(comp_camera->getFront().x / front_length_camera);

      if (comp_camera->getFront().x > 0) {
          if (comp_camera->getFront().z > 0) {
              angle_camera_front = angle_camera_front;
          }
          else {
              angle_camera_front = 2 * PI - angle_camera_front;
          }
      }
      else {
          if (comp_camera->getFront().z > 0) {
              angle_camera_front = angle_camera_front;
          }
          else {
              angle_camera_front = 2 * PI - angle_camera_front;
          }
      }
      float front_length_target = sqrt(player_trans->getFront().x * player_trans->getFront().x + player_trans->getFront().z * player_trans->getFront().z);

      float frontX_player = front_length_target * cos(angle_camera_front);
      float frontZ_player = front_length_target * sin(angle_camera_front);



      VEC3 newFront = VEC3(frontX_player, 0.f, frontZ_player);
      VEC3 hRight = VEC3(-frontZ_player, 0.f, frontX_player);
      newFront.Normalize();

      VEC3 local_dir;
      if (comp_player->getVelState() == velocityStates::SLOWINGDOWN && !comp_player->frenadaInmediata) {
          local_dir = player_trans->getFront();
      }
      else {
          local_dir = newFront * input["move_front"].value + hRight * input["move_side"].value;
      }

      //VEC3 local_dir = newFront * input["move_front"].value + hRight * input["move_side"].value;
      local_dir.Normalize();

      return local_dir;
  }
