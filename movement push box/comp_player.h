#pragma once

#include "components/common/comp_base.h"
#include "entity/common_msgs.h"
#include "entity/entity.h"
#include "engine.h"
#include "sinn/modules/module_physics_sinn.h"
#include "geometry/interpolators.h"
#include "sound/soundEvent.h"

#pragma region Enums and Structs
enum class velocityStates { STOPPED, SPEEDINGUP, SLOWINGDOWN, MAXVELOCITY };

enum class fsmStates {
  IDLE,
  WALK,
  SPRINT,
  STEALTH,
  LONGJUMPSTART,
  LONGJUMPUP,
  LONGJUMPDESC,
  LONGJUMPEND,
  SHORTJUMPSTART,
  SHORTJUMPUP,
  SHORTJUMPDESC,
  SHORTJUMPEND
};

enum enumCameras {
  CAM3D, CAM2D, FLYOVER
};

enum EInteractionType {
  UNDEFINED = 0,
  BOX,
  COLUMN,
  ROPE,
  MATTER,
  LIGHT_SWITCH,
  PLANT,
  SWITCH,
  CHANGE_CAMERA
};

struct CameraData {
  std::string name;
  float time;
  CameraData() {}
  CameraData(std::string n, float t) {
    name = n;
    time = t;
  }
};

struct lastMovement {
    VEC3 deltaMove = VEC3::Zero;
    float dYaw = 0;
    float dPitch = 0;
    float dRoll = 0;

    bool applied = false;

    void reset() {
        deltaMove = VEC3::Zero;
        dYaw = 0;
        dPitch = 0;
        dRoll = 0;
        applied = false;
    }
};

struct pathRecord {
    std::string actor_name;
    float abs_time;
    VEC3 pos_player;
};
#pragma endregion


class TCompPlayer : public TCompBase {


#pragma region Component
private:
  DECL_SIBLING_ACCESS();
  bool Materia = false;
  void updateTimers(float dt);

  float sonidoPies = 0;
  float lastTimeSonidoPies=0;
  float diferenciaPasos = 0;
public:
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  void renderDebug();
  static void registerMsgs();
  void update(float dt);
  void onEntityCreated();

  bool getMateria() { return Materia; };
  void setMateria(bool mat) {  Materia = mat;   };
  void sendMsgCajones(std::string n_ent);
#pragma endregion

#pragma region Movement
private:

  SoundEvent* eventoRight = nullptr;
  SoundEvent* eventoLeft = nullptr;
  bool in_sprint = false;
  bool in_walk = false;

  float timer_player = 0.f;

  float time_ini_acceleration = 0.f;  // Tiempo cuando empezo acelerar
  float time_ini_deceleration = 0.f;   // Tiempo cuando empezo a desacelerar
  float time_ini_colDown = 0.f;  //Tiempo cuando empezo a caer
  float time_beforeJumpDesc = 0.1f;  //Tiempo que esperaremos antes de caer cuando collision down es false
  float time_ini_jump = 0.f;
  float y_ini_value = 0.f;  // Valor de y al comenzar el salto

  velocityStates velState = velocityStates::STOPPED;

  VEC3 velocity = VEC3::Zero;
  float modVelocity = 0.f;
  VEC3 local_dir = VEC3::Zero;

  float rotation_speed = 0.f;
  float jump_strength = 0.f;

  float max_y_hor_jump = 1.5f;
  float max_y_vert_jump = 2.0f;
  //float max_jump_velocity = 1.0f;
  float time_jump = 0.8f;

  bool changeSecVel = false;
  bool vertical_jump = false;


  // Variables formulas salto
  float j_a = 0.f;
  float j_x0 = 0.f;
  float j_y0 = 0.f;
  int j_n = 2;  // Grado de la ecuacion del salto
  ///////////////////////////


  //STEP MANAGER
  VEC3 playerStepPos = VEC3::Zero;
  bool stepsActivated = false;
  float stepDistanceLeft = 0.f;
  float stepDistanceRight = 0.f;
  bool leftFootInFloor = false;
  bool rightFootInFloor = false;
  SoundEvent* steps_event = nullptr;

  CModulePhysicsSinn::MoveCollisions collisions;
  bool dead = false;
  bool fallingDead = false;
  bool deadAnimation = false;
  bool firstTime = true;
  float fadeTimer = 0;
  bool showWindow = false;

  float max_fall_damage = 25.f;
  float fall_damage = 0.f;

  // Rotate in Time
  CTimer time;
  float rot_angle = 0.f;
  float rot_time = 0.f;
  const float k = 3.f;
  const float fk = 0.26988f;
  float global_acc = 1.f;

public:
  void onConstrict(const TMsgConstrict& msgConstrict);
  void onDead(const TMsgDeath& msgDead);
  void onFallingDead(const TMsgDeathFall& msgDead);
  bool isDead() { return dead; }
  bool isFallingDead() { return fallingDead; }
  bool isDeadAnimation() { return deadAnimation; }
  void resetPlayer();

  bool isWalking() { return in_walk; };
  bool isSprinting() { return in_sprint; };
  void setWalking(bool walk) { in_walk = walk; };
  void setSprinting(bool sprint) { in_sprint = sprint; };

  void activateSteps(float distanceLeft, float distanceRight, bool n);
  void playSteps();
  SoundEvent* getStepsEvent() { return steps_event; };

  const VEC3 getVelocity() { return velocity; }
  void setVelocity(VEC3 value) { velocity = value; }  
  const float getModVelocity() { return modVelocity; }
  void setModVelocity(float value) { modVelocity = value; }
  const VEC3 getLocalDir() { return local_dir; }
  void setLocalDir(VEC3 value) { local_dir = value; }

  float getIniTimeAcc() { return time_ini_acceleration; }
  float getIniTimeDec() { return time_ini_deceleration; }
  void setIniTimeAcc() { time_ini_acceleration = timer_player; }
  void setIniTimeDec() { time_ini_deceleration = timer_player; }

  velocityStates getVelState() { return velState; }
  void setVelState(velocityStates s) { velState = s; }
  float getPlayerTimer() { return timer_player; }

  float getIniTimeColDown(){ return time_ini_colDown; }
  void setIniTimeColDown() { time_ini_colDown = timer_player; }
  bool isFallingDown() { return !collisions.collision_down && timer_player - time_ini_colDown > time_beforeJumpDesc; }

  void precalculateFuncJump();
  float calculateJump();
  void initializeJump();
  void finalizeJump();
  float getIniTimeJump() { return time_ini_jump; }
  bool isJumping() { return collisions.jumping; }
  bool isPushingUp();
  bool isCollisionUp();

  void setChangeSecVel(bool value) { changeSecVel = value; }
  bool getIfChangeSecVel() { return changeSecVel; }
  void setVerticalJump(bool value) { vertical_jump = value; }
  /*void setMaxJumpVelocity(float vel) { max_jump_velocity = vel; }
  float getMaxJumpVelocity() { return max_jump_velocity; }*/

  const bool getCollisionDown() { return collisions.collision_down; }
  void setCollisionDown(bool value) { collisions.collision_down = value; }
  void setJumpStrength(float value) { jump_strength = value; }
  const float getJumpStrenght() { return jump_strength; }

  void rotateWithValues(float dYaw, float dPitch, float dRoll);
  void rotate(float dt);
  void rotateInTime(float dt);
  void initRotateInTime(VEC3 dst, float time);
  void forceRotation(VEC3 local_dir);
  void addAnimRot();
  void move(float dt);
  void moveObj2D(float dt);
#pragma endregion

#pragma region Noise
private:
  //Variables sistema generacion de ruido
  bool stealth_mode = false;
  float noise = 0.f;
  float max_noise_value = 2;
  float variation_value = 1.5;
  float change_point = 2;
  ////////////////////////////////////

  //VARIABLES PARA MOSTRAR EN EL MENU DE STATUS
  std::deque<float> qGenerated_noise; // Cola con el ruido que vamos generando
  bool windowStatus_active = false;
  CTimer timeGame = CTimer();
  bool have_been_detected = false; // Variable booleana para saber cuando hemos recibido mensaje del ruido que el enemigo detecta de nosotros
  bool detected = false; // Variable booleana para saber cuando hemos sido detectados
  float cycle = 0.1f; // segundos que pasaran para hacer un ciclo de onda
  float freq = (float)M_PI / cycle;
  float noise_values[200];

public:
  void setNoise(float velocity, float mod);
  float getNoise() { return noise; }
#pragma endregion

#pragma region Cameras
private:
  CHandle h_camera = CHandle();
  CameraData cameraData;
  int modoCamera = CAM3D;
  CModuleCameraMixer& mixer = CEngine::get().getCameraMixer();
  bool flyover_active = false;

  bool init_cameras = true;
  bool focus2shadow = true;

  bool canChangeCamera = true;  // Determina si podemos o no cambiar la camara en el mixer


public:
  CHandle getCamera() {
    return h_camera;
  }

  CameraData getCameraData() { return cameraData; }
  void setCameraData(CameraData _camera) { cameraData = _camera; }
  void changeCamera(CameraData fromCamera, CameraData toCamera);
  void putFlyOverCamera();

  int getModoCamera() { return modoCamera; }
  void setModoCamera(int _modoCamera) { modoCamera = _modoCamera; }

  bool focusToShadow() { return focus2shadow; }
  //void switchFocusToShadow() { focus2shadow = !focus2shadow; }
  void switchFocusToShadow();

  float getPitchCamera();
  float getYawCamera();

  void setCanChangeCamera(bool b) { canChangeCamera = b; }
#pragma endregion

#pragma region Projection
private:
  TCompLight* light = nullptr;
  VEC3 wall_point = VEC3::Zero;
  VEC3 wall_normal = VEC3::Zero;

  float velocity_2D_offset = 30.f;
  float velocity_2D_light = 7.f;

  float time_ini_moveSide = 0.f;
  float time_ini_moveFront = 0.f;

  bool projected = false;

  fsmStates state_before_proj = fsmStates::IDLE;

public:
  VEC3 oldInputDir2D = VEC3::Zero;
  float timeResetProject = 2;
  float timer_ResetProject = 0;

  bool project();
  void desproject();

  bool isProjected();

  VEC3 getWallPoint() { return wall_point; }
  VEC3 getWallNormal() { return wall_normal; }
  bool setProjectedWall(VEC3 Velocity = VEC3(0, 0, 0), float dt = 0);

  TCompLight* getLight() { return light; }

  float getVelocity2D_Offset() { return velocity_2D_offset; }
  float getVelocity2D_Light() { return velocity_2D_light; }

  void setTimeIniMoveSide() { time_ini_moveSide = timer_player; }
  void setTimeIniMoveFront() { time_ini_moveFront = timer_player; }
  float getTimeIniMoveSide() { return time_ini_moveSide; }
  float getTimeIniMoveFront() { return time_ini_moveFront; }

  void setLastState(std::string s);
  fsmStates getLastState() { return state_before_proj; }
#pragma endregion

#pragma region Interaction
private:
  CHandle ent_interact = CHandle();
  int interact_type = UNDEFINED;
  CHandle plant = CHandle();
public:
  void setPlant(CHandle h) { plant = h; };
  CHandle getPlant() {    return plant;  };
  CHandle getInteractionEntity() { return ent_interact; }
  int getInteractionType() { return interact_type; }
  void setInteractionVars(CHandle h_ent, int type) {
    ent_interact = h_ent;
    interact_type = type;
  }
#pragma endregion

#pragma region Move Box
private:
  CHandle push_box = CHandle();
  CModulePhysicsSinn::MoveCollisions box_collisions;
  SoundEvent* move_box_sound = nullptr;
  VEC3 startPos = VEC3::Zero;

  //bool intoTrigger = false;

  //Parametros para MoveObj2D
  VEC3 oldPos = VEC3(0,0,0);
  bool movePlayer;
  float diff = 0;

public:

  void hitBox(const TMsgPush& msg_push);
  CHandle getPushBox() { return push_box; };
  bool canMoveTheBox();
  SoundEvent* getMoveBoxSound() { return move_box_sound; };
  void nullMoveBoxSound() { move_box_sound = nullptr; };
  void convertShadowPushBox(bool create_controller);
  void convertPushBox(bool controller);
  void movePushBox(float dt);
  bool checkPushBox() {
    CEntity* ent_push_box = push_box;
    if (!ent_push_box) return false;
    TCompCollider* push_box_col = ent_push_box->get<TCompCollider>();
    if (!push_box_col) return false;
    if (!push_box_col->boxcontroller) return false;
    else return true;
  }
  //bool getInFrontEntity() { return intoTrigger; };

  bool checkShadowPushBox();
#pragma endregion
  
  VEC3 getCheckPointPos() { return startPos; };
  void saveGame(const TMsgCheckpoint& msg);


#pragma region Debug / Test
public:
  float tSpeed = 0.f;
  VEC3 ini = VEC3::Zero;
  VEC3 fin = VEC3::Zero;
  float distxSec = 0.f;
  float distMaxWalk = 0.f;
  std::string db_state = "";
  VEC3 dbg_resistance = VEC3::Zero;
  float dbg_velocity_lenght = 0.f;
  VEC3 pointToMove = VEC3::Zero;
  bool drawJump = false;
  float timer_debug_jump = 0;
  bool frenadaInmediata = false;
  bool vel2DFija = true;

  std::vector<VEC3> debug_pos_jump;
  void insertPosJump();
  void startDrawJump();
#pragma endregion

#pragma region Death
private:
  float rebirth_time = 0.f;
  float current_time = 0.f;
  bool rebirth = true;
  bool reset = false;
  float time_eternalFalling = 15;
  float time_deathByFall = 2;

public:
  float timer_FallingState = 0;

  float isRebirth() { return rebirth; }
  void setRebirth(float time);

  float getTimeEternalFalling() { return time_eternalFalling; }
  float getTimeDeathByFall() { return time_deathByFall; }

  float ProgressBar(const char* optionalPrefixText, float value, const float minValue, const float maxValue, const char* format, const ImVec2& sizeOfBarWithoutTextInPixels, const ImVec4& colorLeft, const ImVec4& colorRight, const ImVec4& colorBorder);
#pragma endregion

#pragma region PathDebug

  std::vector<pathRecord> list_steps;

  bool startRecord = false;
  float recordTimer = 0;

  void hitWithActor(const TMsgRecordPath& msg);



#pragma endregion

#pragma region OnPlatform

  bool isOnTopPlatform = false;

  lastMovement lastM;

  void OnPlatform(const TMsgPlatMove& msg);
  void moveWithPlatform(float dt);

#pragma endregion

#pragma region finalEvent

  bool final_event_active = false;
  float timer_fEv = 0;
  float fEv_time = 120;

  void initFinalEvent(const TMsgInitFinalEvent& msg);
  void updateFinalEvent();

#pragma endregion

};
