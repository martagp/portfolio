#include "mcv_platform.h"

#include "particles_system.h"
#include "particles_emitter.h"
#include "render/textures/texture.h"
#include "render/render_module.h"
#include "engine.h"
#include "components/common/comp_transform.h"
#include "entity/entity.h"

#include "skeleton/comp_skeleton.h"

namespace particles
{
  CSystem::CSystem()
  {
    _combinativeTechnique = Resources.get("particles.tech")->as<CTechnique>();
    _additiveTechnique = Resources.get("particles_additive.tech")->as<CTechnique>();
    _quadMesh = Resources.get("quad_xy.mesh")->as<CMesh>();
  }

  void CSystem::start(float warmUpTime)
  {
    if (!_active)
    {
      _active = true;
      interval = _emitter->interval;
      numParticles = _emitter->numParticles;
      emit();
      
      if (warmUpTime > 0.f)
      {
        constexpr float kStep = 1.f / 60.f;
        while (warmUpTime > 0.f)
        {
          const float stepTime = std::min(kStep, warmUpTime);
          update(stepTime);
          warmUpTime -= stepTime;
        }
      }
    }
  }
  void CSystem::fadeOut(float duration)
  {
    _fadeDuration = duration;//multiplico por dos si no empieza el fade demasiado pronto (falta ver why)
    _fadeTime = 0.f;
  }

  void CSystem::updateFading(float delta) {

    if (_fadeDuration != 0.f)
    {
      _fadeTime += delta;
      _fadeRatio = (1.f - (_fadeTime / _fadeDuration));
    }
    else {
      _fadeRatio = 1.0f;
    }
  }
  void CSystem::emit()
  {
    // remove old particles
    while (_particles.size() + numParticles > _emitter->maxParticles)
    {
        if (_particles.size() > 0) _particles.pop_front();
        else break;
    }

    const MAT44& ownerTransform = getOwnerTransform().asMatrix();
    
    for (int i = 0; i < numParticles; ++i)
    {
      TParticle p = generate();

      if (!_emitter->followOwner)
      {
        p.position = VEC3::Transform(p.position, ownerTransform);
      }

      _particles.push_back(std::move(p));
    }

    _time = 0.f;
  }
  float RandomFloat(float max, float min) {
    
    float random = ((float)rand()) / (float)RAND_MAX;

    // generate (in your case) a float between 0 and (4.5-.78)
    // then add .78, giving you a float between .78 and 4.5
    float range = max - min;
    return (random * range) + min;
  }

  void CSystem::update(float elapsed)
  {
    if (!_active)
    {
      return;
    }
    updateFading(elapsed);
    VEC3 gravity;
    MAT44 world = MAT44::Identity;
    MAT44 world_rot = MAT44::Identity;
    TCompSkeleton* skeleton = nullptr;
    if (_emitter->followOwner) {
      CEntity* e = _owner;
      TCompTransform* e_transform = e->get<TCompTransform>();
      world = e_transform->asMatrix();
      
      VEC3 proj_vector = e_transform->getFront();
      world_rot = MAT44::CreateFromQuaternion(e_transform->getRotation());
      
    }
    gravity= VEC3(0.f, -9.8f, 0.f);
  
    for (auto& p : _particles)
    {
      p.time += elapsed;

      if(_emitter->lifetime != 0.f && p.time >= p.duration) continue;

      const float lifetimeRatio = p.duration != 0.f ? std::clamp(p.time / p.duration, 0.f, 1.f) : 0.f;

      p.velocity += gravity * _emitter->gravityFactor * elapsed;
      if (_emitter->followOwner)p.velocity += VEC3::Transform(_emitter->tvelocity.get(lifetimeRatio), world_rot)*elapsed;
      
      p.position += p.velocity * elapsed;
      
      p.color = _emitter->colors.get(lifetimeRatio)*_fadeRatio;
      p.size = _emitter->sizes.get(lifetimeRatio);

     if (_emitter->noise_strength > 0)//some particles will have a noise to them so they're more "natural"
      {
        p.random_direction = VEC3(p.random_direction.x + (2.0f * ((float)rand() / (float)RAND_MAX) - 1.0f), p.random_direction.y + (2.0f * ((float)rand() / (float)RAND_MAX) - 1.0f), p.random_direction.z + (2.0f * ((float)rand() / (float)RAND_MAX) - 1.0f));
        p.velocity += p.random_direction * _emitter->noise_strength * elapsed;
      }
    }

    // remove dead particles
    auto eraseIt = std::remove_if(_particles.begin(), _particles.end(), [](TParticle& p){ return p.duration != 0.f && p.time >= p.duration; });
    _particles.erase(eraseIt, _particles.end());

    // shall we emit again?
    _time += elapsed;
    if (interval > 0.f && _time >= interval)
    {
      if (_emitter->maxInterval > 0.0f)//check if the interval we want is constant or in a range
      {
        interval = RandomFloat(_emitter->maxInterval, _emitter->minInterval);
      }
      if (_emitter->randomParticles)//check if the num
      {
        numParticles = rand() % _emitter->numRandMaxParticles + _emitter->numParticles;
      }
      else {
        numParticles = _emitter->numParticles;
      }
      emit();
    }
  }

  void CSystem::render()
  {
    if (!_active)
    {
      return;
    }
    const CTechnique* tech = _emitter->additive ? _additiveTechnique : _combinativeTechnique;
    tech->activate();

    _emitter->texture->activate(TS_ALBEDO);

    CEntity* eCamera = CEngine::get().getRender().getCamera();
    if(!eCamera) return;

    TCompTransform* cCameraTransform = eCamera->get<TCompTransform>();
    if (!cCameraTransform) return;

    const VEC3 cameraPosition = cCameraTransform->getPosition();
    const VEC3 cameraUp = cCameraTransform->getUp();

    const MAT44& ownerTransform = getOwnerTransform().asMatrix();

    for (auto& p : _particles)
    {
       VEC3 position = _emitter->followOwner ? VEC3::Transform(p.position, ownerTransform) : p.position;
      if (_emitter->followAnimation) {
        CEntity* e = _owner;
        TCompSkeleton* skeleton =e-> get<TCompSkeleton>();
        string name = e->getName();
        QUAT rot = skeleton->getAbsRotationBone(1);
        MAT44 sk = MAT44::CreateScale(1)
          * MAT44::CreateFromQuaternion(rot)
          * MAT44::CreateTranslation(skeleton->getAbsTraslationBone(1));
        position = VEC3::Transform(p.position, sk);
      }
      const MAT44 sc = MAT44::CreateScale(p.size);

      const MAT44 bb = MAT44::CreateBillboard(position, cameraPosition, cameraUp);
      const MAT44 world = sc * bb ;

      setObjRenderCtes(world, p.color);

      _quadMesh->activateAndRender();
    }
  }


  CTransform& CSystem::getOwnerTransform() const
  {
    CEntity* eOwner = _owner;
    if (eOwner)
    {
      TCompTransform* cTransform = eOwner->get<TCompTransform>();
      if (cTransform)
      {
        return *cTransform;
      }
    }

    static CTransform dummy;
    return dummy;
  }
  TParticle CSystem::generate() const
  {
    TParticle p;

    MAT44 world = MAT44::Identity;
    MAT44 world_rot = MAT44::Identity;
    TCompTransform* e_transform = nullptr;
    
    TCompSkeleton* skeleton = nullptr;
    if (_emitter->followOwner) {
      CEntity* e = _owner;
      e_transform = e->get<TCompTransform>();
      world = e_transform->asMatrix();
      world_rot = MAT44::CreateFromQuaternion(e_transform->getRotation());
     
    }
    
    p.color = _emitter->colors.get(0.f) + randomFloat(-1.f, 1.f) * _emitter->colorVariation;
    p.size = _emitter->sizes.get(0.f);
    p.duration = _emitter->lifetime + randomFloat(-_emitter->lifetimeVariation, _emitter->lifetimeVariation);
    p.time = 0.f;
    if (_emitter->followOwner)p.velocity = VEC3::Transform(p.velocity, world_rot);
    else p.velocity = _emitter->velocity + randomFloat(-1.f, 1.f) * _emitter->velocityVariation;
    
    p.random_direction = _emitter->AddNoiseOnAngle(-180, 180);
    p.rotation = _emitter->rotation;
    
    switch (_emitter->type)
    {
    case CEmitter::EType::Point:
    {
      p.position = _emitter->position;
      if (_emitter->followOwner) p.position = e_transform->getPosition();
      break;
    }
    case CEmitter::EType::Line:
    {
      p.position = _emitter->position;
      if (_emitter->followOwner) p.position = e_transform->getPosition();
      const float& length = std::get<float>(_emitter->typeParams);
      p.position.x += randomFloat(-length, length);
      break;
    }
    case CEmitter::EType::Plane:
    {
      p.position = _emitter->position;
      const VEC2& area = std::get<VEC2>(_emitter->typeParams);
      p.position.x += randomFloat(-area.x, area.x);
      p.position.y += randomFloat(-area.y, area.y);
      break;
    }
    case CEmitter::EType::Cube:
    {
      p.position = _emitter->position;
      const VEC3& volume = std::get<VEC3>(_emitter->typeParams);
      p.position.x += randomFloat(-volume.x, volume.x);
      p.position.y += randomFloat(-volume.y, volume.y);
      p.position.z += randomFloat(-volume.z, volume.z);
      break;
    }
    case CEmitter::EType::Circle:
    {
      p.position = _emitter->position;
      const float& radius = std::get<float>(_emitter->typeParams);
      p.position += yawToVector(randomFloat(-M_PI, M_PI)) * randomFloat(0.f, radius);
      break;
    }
    case CEmitter::EType::Sphere:
    {
      p.position = _emitter->position;
      
      
      const float& radius = std::get<float>(_emitter->typeParams);
      p.position += yawPitchToVector(randomFloat(-M_PI, M_PI), randomFloat(-M_PI, M_PI)) * randomFloat(0.f, radius);
      break;
    }
    case CEmitter::EType::Cone:
    {
      p.position = _emitter->position;
      const VEC2& params = std::get<VEC2>(_emitter->typeParams);
      p.position += yawToVector(randomFloat(-M_PI, M_PI)) * randomFloat(0.f, params.x); // radius
      p.position.y += randomFloat(0.f, params.y);
      break;
    }
    }

    return p;
  }
}
