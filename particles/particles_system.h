#pragma once

#include "particle.h"

namespace particles
{
  class CEmitter;

  class CSystem
  {
  public:
    CSystem();

    void start(float warmUpTime = 0.f);
    void updateFading(float delta);
    void emit();
    void update(float elapsed);
    void render();
    TParticle generate() const;

    void setActive(bool a) { _active = a; };
    void setEmitter(const CEmitter* emitter) { _emitter = emitter; }
    void setOwner(CHandle owner) { _owner = owner; }

    void fadeOut(float duration);

  private:
    CTransform& getOwnerTransform() const;

    const CEmitter* _emitter = nullptr;
    CHandle _owner;
    std::deque<TParticle> _particles;
    bool _active = false;
    float _time = 0.f;

    int numParticles = 0;
    float interval = 0;

    const CTechnique* _combinativeTechnique = nullptr;
    const CTechnique* _additiveTechnique = nullptr;
    const CMesh* _quadMesh = nullptr;


    //fade
    float _fadeDuration = 0.f;
    float _fadeTime = 0.f;
    float _fadeRatio = 0.f;
  };
}

