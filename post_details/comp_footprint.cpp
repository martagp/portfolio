#pragma once
#include <mcv_platform.h>
#include "comp_footprint.h"
#include "components/common/comp_transform.h"
#include "sinn/modules/module_physics_sinn.h"
#include "components/common/comp_particles.h"
#include "components/common/comp_render.h"
DECL_OBJ_MANAGER("footprint", TCompFootprint);

void TCompFootprint::startFootprintRender()
{
	on = true;
	timer = 0.f;
	TCompRender* trend = get<TCompRender>();
	trend->activateState(1);
	TCompParticles* particles = get<TCompParticles>();
	particles->setFade(maxTime);

	TMsgPauseParticles msg;
	msg.pause = false;
	CEntity* e = CHandle(this).getOwner();
	e->sendMsg(msg);


}

void TCompFootprint::stopFootprintRender()
{
	on = false;
	TCompRender* trend = get<TCompRender>();
	trend->activateState(0);
	TMsgPauseParticles msg;
	msg.pause = true;
	CEntity* e = CHandle(this).getOwner();
	e->sendMsg(msg);
}

void TCompFootprint::debugInMenu()
{
}

void TCompFootprint::load(const json& j, TEntityParseContext& ctx)
{
	is_left = j.value("is_left", is_left);
}

void TCompFootprint::update(float dt)
{

	if (on) {
		timer += dt;
		if (timer > (maxTime)) {
			stopFootprintRender();
		}
	}
}

void TCompFootprint::renderDebug()
{
	TCompTransform* t = get<TCompTransform>();
	
	
	MAT44 Z2Y = MAT44::CreateRotationZ((float)-M_PI_2);
	
	
	//drawWiredSphere(t->getPosition(), 0.1, is_left ? Color::Blue : Color::Green);
}

void TCompFootprint::onEntityCreated()
{
	CHandle controller = getEntityByName("Footprint_controller");
	assert(controller.isValid());
	TMsgRegisterFootprint msg;
	msg.foot = CHandle(this).getOwner();
	msg.is_left = is_left;
	controller.sendMsg(msg);
}

void TCompFootprint::registerMsgs()
{
}
