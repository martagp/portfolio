#pragma once
#include <mcv_platform.h>
#include "entity/entity.h"
#include "components/common/comp_base.h"
#include "entity/common_msgs.h"

using namespace std;

class TCompFootprint : public TCompBase
{

	DECL_SIBLING_ACCESS();
	float timer =0.f;
	bool is_left = false;
	bool on = false;
	float maxTime = 5.f;
public:

	void startFootprintRender();
	void stopFootprintRender();
	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void renderDebug();
	void onEntityCreated();
	static void registerMsgs();
};