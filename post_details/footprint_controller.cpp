#include "mcv_platform.h"
#include "footprint_controller.h"
#include "engine.h"
#include "modules/module_entities.h"
#include "components/common/comp_transform.h"
#include "sinn/characters/player/comp_player.h"
#include "comp_footprint.h"
#include "sinn/components/comp_audio.h"
#include "sound/soundEvent.h"

DECL_OBJ_MANAGER("footprint_controller", CFootprint_Controller);
void CFootprint_Controller::registerFootprints(const TMsgRegisterFootprint& msg)
{

	footprint aux;
	aux.h_footprint = msg.foot;
	aux.is_left = msg.is_left;
	aux.pos = playerOldPos;
	footprints.push_back(aux);

}

void CFootprint_Controller::debugInMenu()
{
}

void CFootprint_Controller::load(const json& j, TEntityParseContext& ctx)
{
}

void CFootprint_Controller::update(float dt)
{
	CEntity* e_player = h_player;
	TCompTransform* t_player = e_player->get<TCompTransform>();
	if (!pause) {//it unpauses when the player is on shadow mode
		float step_distance = 1000.f;
		TCompPlayer* p_player = e_player->get<TCompPlayer>(); 
		if (p_player->isSprinting()) step_distance = 1.3;
		else if (p_player->isWalking()) step_distance = 1;
		if ( VEC3::Distance(playerOldPos, t_player->getPosition()) > step_distance) {

			//me guardo la nueva posicion como la antigua 
			playerOldPos = t_player->getPosition();

			//me guardo el paso en un aux
			footprint aux = footprints.back();
			VEC3 offset= t_player->getLeft();;
			if (!aux.is_left)		offset = -offset;//el offset aplicado en funcion si son huellas derecha o izq
			offset *= 0.1;//el getLeft lleva a la huella a cancun, le bajo el offset para que no ocurra
			aux.pos = playerOldPos+offset;//guardo la posicion por si mas adelante la necesito en el vector pa algo (diogenes)
			
			
			CEntity* e_foot = aux.h_footprint;
			//le aplico la posicion nueva y la rotacion del player
			TCompTransform* t_foot = e_foot->get<TCompTransform>();
			t_foot->setPosition(aux.pos);
			t_foot->setRotation(t_player->getRotation());

			//llamo al componente huella a que se renderice todo, aplicando un temporizador que lo maneja ese componente
			TCompFootprint* foot = e_foot->get<TCompFootprint>();
			foot->startFootprintRender();

			TCompAudio* audio = get<TCompAudio>();
			SoundEvent* s = audio->playEvent("2Dpasos");
			s->setVolume(0.2f);
			//borro el ultimo paso y lo meto en el principio del duque con ayuda del auxiliar
			footprints.pop_back();
			footprints.emplace_front(aux);

		}
	}
	else {
		//si estamos en 3D llamo a que dejen de renderizarse todas las huellas
		for (auto i : footprints) {
			CEntity* e = i.h_footprint;
			TCompFootprint* foot = e->get<TCompFootprint>();
			foot->stopFootprintRender();
	
		}
	}
	
	
}

void CFootprint_Controller::onEntityCreated()
{
	h_player = CEngine::get().getEntities().getPlayerHandle();
	CEntity* e_player = h_player;
	TCompTransform* t_player = e_player->get<TCompTransform>();
	playerOldPos = t_player->getPosition();
}

void CFootprint_Controller::registerMsgs()
{
	DECL_MSG(CFootprint_Controller, TMsgRegisterFootprint, registerFootprints);
	DECL_MSG(CFootprint_Controller, TMsgPauseParticles, setPause);
}

void CFootprint_Controller::renderDebug()
{
}
void CFootprint_Controller::setPause(const TMsgPauseParticles& msg)
{
	pause = msg.pause;
	if (!pause) {
		CEntity* e_player = h_player;
		TCompTransform* t_player = e_player->get<TCompTransform>();
		playerOldPos = t_player->getPosition();
	}
}