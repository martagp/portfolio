#pragma once
#include <mcv_platform.h>
#include "components/common/comp_base.h"
#include "entity/common_msgs.h"
#include "entity/entity.h"
#include <string>


using namespace std;
struct footprint {
	CHandle h_footprint;
	VEC3 pos;
	bool is_left;
};
class CFootprint_Controller : public TCompBase
{
	DECL_SIBLING_ACCESS();
private:
	CHandle h_player = CHandle();
	deque<footprint> footprints;
	int numFootprints=0;
	VEC3 playerOldPos = VEC3::Zero;
	bool pause = true;

	void setPause(const TMsgPauseParticles& msg);
	void registerFootprints(const TMsgRegisterFootprint& msg);

public:

	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void onEntityCreated();
	static void registerMsgs();
	void renderDebug();
};