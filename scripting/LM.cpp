#include "mcv_platform.h"
#include <Windows.h>
#include <mmsystem.h>
#include "LM.h"
#include "engine.h"

#include "modules/module_entities.h"
#include "render/render_module.h"
#include "input/input_module.h"
#include "sinn/modules/module_physics_sinn.h"
#include "sinn/modules/module_sound.h"
#include "sinn/modules/module_cutscene.h"
#include "ui/ui_module.h"

#include "components/common/comp_transform.h"
#include "components/common/comp_collider.h"
#include "entity/common_msgs.h"
#include "sinn/characters/player/comp_player.h"
#include "sinn/characters/enemies/comp_enemy3D.h"
#include "sinn/interaction/comp_ui_interaction.h"
#include "sinn/interaction/comp_column.h"
#include "sinn/components/comp_cuerda.h"
#include "sinn/components/comp_checkpoint.h"
#include "sinn/interaction/comp_animation_object.h"
#include "sinn/interaction/comp_movement_object.h"
#include "sound/soundEvent.h"
#include "sinn/components/comp_audio.h"
#include "components/common/comp_tags.h"
#include "sinn/behavior/ai/ai_controller.h"

#include <windowsx.h>
#include "windows/application.h"
static SLB::Manager* m;
static SLB::Script* s;
static std::vector<ScriptTask> DelayedScripts;


LM::LM() {
  printf("Constructor\n");
  if (m == NULL) m = new SLB::Manager; 
  if (s == NULL) s = new SLB::Script(m);
}

void LM::Boot() {
  Bind();
 
}

void LM::HelloWorld() {
    dbg("Hello World\n");
}
void LM::motherlode()
{
    dbg("This is not the Sims you moron, go get a job \n");
}

void LM::dbg(string x)
{
  dbg(x +" \n");
}

bool LM::ExecScript(string str) {
    
    bool exito = false;
  try {
    s->doString(str);
    
    exito = true;
  }
  catch(std::exception x){
      
      printf("error %s\n", x.what());
  }
  
  return exito;
}

void LM::ExecScriptDelayed(string str, unsigned long delay)
{
  ScriptTask st;
  //st.birth = Time.elapsed();
  st.delay = delay;
  st.code = str;
  DelayedScripts.push_back(st);
}

void LM::ExecFile(string str)
{
  try {
    s->doFile(str);
  }
  catch (std::exception x) {
    printf("error %s\n", x.what());

  }
}

void LM::Recalc(float delta)
{
  for (int i = 0; i < DelayedScripts.size(); i++)
  {
     DelayedScripts[i].delay -= delta*1000;


      if (DelayedScripts[i].delay<=0) {
        ExecScript(DelayedScripts[i].code);
        DelayedScripts.erase(DelayedScripts.begin() + i);
        return;
      }
  }
}

void LM::Bind() {
  //Binds the functions and classes  of Lua.
  
    /*PUBLISH ALL CLASSES*/
    
    SLB::Class<LM>("LM", m)
        //a comment/documentation for the class [Optional]
        .comment("LUA LM class")
        //empty constructor, we can also wrapper constructor
        // with arguments using .constructor<TypeArg1,TypeArg2,...>()
        .constructor()
        //a method/function/value...
        .set("ExecScriptDelayed", &LM::ExecScriptDelayed)
        .set("ExecScript", &LM::ExecScript)
        .set("HelloWorld", &LM::HelloWorld)
        .set("motherlode", &LM::motherlode)
        .set("dbg", &LM::dbg)


        
    ;
 
    SLB::Class <CHandle>("CHandle", m)
        .comment("CHandle wrapper")
        .constructor()
        .set("fromUnsigned", &CHandle::fromUnsigned)
        .set("destroy", &CHandle::destroy)
        .set("isValid", &CHandle::isValid)
    ;
    
    SLB::Class< VEC3 >("VEC3", m)
        .constructor<float, float, float>()
        .comment("the VEC3 class")
        .property("x", &VEC3::x)
        .property("y", &VEC3::y)
        .property("z", &VEC3::z)
    ;
    

    SLB::Class <SoundEvent>("SoundEvent", m)
      .comment("SoundEvent wrapper")
      .set("isValid", &SoundEvent::isValid)
      .set("restart", &SoundEvent::restart)
      .set("stop", &SoundEvent::stop)
      .set("setPaused", &SoundEvent::setPaused)
      .set("setVolume", &SoundEvent::setVolume)
      .set("setPitch", &SoundEvent::setPitch)
      .set("setParameter", &SoundEvent::setParameter)
      .set("getPaused", &SoundEvent::getPaused)
      .set("getVolume", &SoundEvent::getVolume)
      .set("getPitch", &SoundEvent::getPitch)
      .set("getParameter", &SoundEvent::getParameter)
      .set("is3D", &SoundEvent::is3D)
      .set("isRelativeToCameraOnly", &SoundEvent::isRelativeToCameraOnly)
      .set("setIsRelativeToCameraOnly", &SoundEvent::setIsRelativeToCameraOnly)
      .set("isPlaying", &SoundEvent::isPlaying)
      ;


    /*PUBLISH ALL FUNCTIONS*/

    //gets
    m->set("getPlayerHandle", SLB::FuncCall::create(&getPlayerHandle));
    m->set("getLocationByHandle", SLB::FuncCall::create(&getLocationByHandle));
    m->set("getHandleByName", SLB::FuncCall::create(&getHandleByName));
    m->set("getCheckpoint", SLB::FuncCall::create(&getCheckpoint));
    m->set("getLastCheckPoint", SLB::FuncCall::create(&getLastCheckPoint));

    //spawns
    m->set("spawnEnemy", SLB::FuncCall::create(&spawnEnemy));
    m->set("spawn", SLB::FuncCall::create(&spawn));
    m->set("UI", SLB::FuncCall::create(&UI));

    // Generic Interaction
    m->set("onInteractionTrigger", SLB::FuncCall::create(&onInteractionTrigger));
    m->set("onInteraction", SLB::FuncCall::create(&onInteraction));

    //column
    m->set("sendInteractionMessage", SLB::FuncCall::create(&sendInteractionMessage));
    m->set("sendInteractionMsgToPlayer", SLB::FuncCall::create(&sendInteractionMsgToPlayer));
    m->set("removeTriggerByEntity", SLB::FuncCall::create(&removeTriggerByEntity));
    m->set("isColumn", SLB::FuncCall::create(&isColumn));

    //Checks
    m->set("isCheckpoint", SLB::FuncCall::create(&isCheckpoint));
    m->set("isPlayer", SLB::FuncCall::create(&isPlayer));
    m->set("isWater", SLB::FuncCall::create(&isWater));
    //remove
    m->set("removeEntityByName", SLB::FuncCall::create(&removeEntityByName));

    //Dead
    m->set("isDeadZone", SLB::FuncCall::create(&isDeadZone));
    m->set("sendMessageDead", SLB::FuncCall::create(&sendMessageDead));
    m->set("sendMessageFallingDead", SLB::FuncCall::create(&sendMessageFallingDead));

    //End
    m->set("sendMessageEnd", SLB::FuncCall::create(&sendMessageEnd));
    m->set("Exit", SLB::FuncCall::create(&Exit));
    m->set("fadeOut", SLB::FuncCall::create(&fadeOut));
    m->set("fadeIn", SLB::FuncCall::create(&fadeIn));


    //utils
    m->set("teleportPlayer", SLB::FuncCall::create(&teleportPlayer));
    m->set("tp", SLB::FuncCall::create(&tp));

    //sound
    m->set("playSound", SLB::FuncCall::create(&playSound));
    m->set("isAudioTrigger", SLB::FuncCall::create(&isAudioTrigger));
    m->set("triggerAudio", SLB::FuncCall::create(&triggerAudio));
    m->set("playBSO", SLB::FuncCall::create(&playBSO));
    m->set("sendContactWater", SLB::FuncCall::create(&sendContactWater));
    m->set("isEnemyTrigger", SLB::FuncCall::create(&isEnemyTrigger));
    m->set("playTemple", SLB::FuncCall::create(&playTemple));
    
    //cutscenes
    m->set("isCutsceneTrigger", SLB::FuncCall::create(&isCutsceneTrigger));
    m->set("playCutscene", SLB::FuncCall::create(&playCutscene));

}

CHandle getPlayerHandle()
{
    return CEngine::get().getEntities().getPlayerHandle();
}

VEC3 getLocationByHandle(CHandle handle)
{
    CEntity* entity = handle;
    TCompTransform* transform = entity->get<TCompTransform>();
    dbg("The location of %s is: %f, %f, %f\/n", entity->getName(), transform->getPosition().x, transform->getPosition().y, transform->getPosition().z);
    return transform->getPosition();
}

CHandle getHandleByName(string s)
{
    return getEntityByName(s);
}

TCompTransform* getTransform(CHandle handle)
{
    CEntity* e = handle;
    TCompTransform* t = e->get<TCompTransform>();
    return t;
}

void getCheckpoint(string name, string Player)
{
  CEntity* e = getEntityByName(name);
  CEntity* p = getEntityByName(Player);

  if (!e->get<TCompCheckpoint>().isValid()) return;
  if (!p->get<TCompPlayer>().isValid()) return;

  TCompPlayer* c_p = e->get<TCompPlayer>();

  TCompCheckpoint* c_check = e->get<TCompCheckpoint>();

  TMsgCheckpoint msg;
  msg.check_point = c_check->getCheckPoint();
  p->sendMsg(msg);
  TMsgSaveState msg_save;
  getObjectManager<CEntity>()->forEach([&msg_save](CEntity* e) {
    e->sendMsg(msg_save);
    });

  CEngine::get().getUI().startAutoSave();

  CHandle(e).destroy();
}

CHandle spawnEnemy(string enemy, VEC3 pos, VEC3 lookDirection)
{
    TEntityParseContext myContext;
    parseScene("data/prefabs/" + enemy + ".json", myContext);
    CHandle h = myContext.entities_loaded[0];
    CEntity* e = h;
    TCompTransform* e_pos = e->get<TCompTransform>();
    e_pos->lookAt(pos, lookDirection);
    return h;
}

CHandle spawn(string obj, VEC3 pos)
{
    TEntityParseContext myContext;
    parseScene("data/prefabs/" + obj + ".json", myContext);
    CHandle h = myContext.entities_loaded[0];
    CEntity* e = h;
    TCompTransform* e_pos = e->get<TCompTransform>();
    e_pos->setPosition(pos);
    return h;
}

CHandle UI(string obj)
{
  TEntityParseContext myContext;
  parseScene("data/scenes/ui/" + obj + ".json", myContext);
  CHandle h = myContext.entities_loaded[0];
  return h;
}

void onInteractionTrigger(string entity_name, bool enter) {
  CEntity* ent = getHandleByName(entity_name);
  TMsgOnTrigger msg;
  msg.trigger_enter = enter;
  ent->sendMsg(msg);
}

void onInteraction(string entity_name) {
  CEntity* ent = getHandleByName(entity_name);
  ent->sendMsg(TMsgOnInteraction());
}

void sendInteractionMessage(string entity_name) {
    CEntity* e = getEntityByName(entity_name);
    TMsgInteract msg;
    msg.push = true;
    e->sendMsg(msg);
}

bool checkColumnInput()
{
  auto &input = CEngine::get().getInput();
  return input["interact"].isPressed();
}

void sendInteractionMsgToPlayer(string ent_name, bool trigger_enter)
{
  //dbg("sendInteractionMsgToPlayer. Entity: %s\n", ent_name.c_str());
  CEntity* player = getPlayerHandle();
  CEntity* ent_interact = getEntityByName(ent_name);
  if (ent_interact == NULL) {
    // Hemos salido del trigger
    assert(!trigger_enter);
    TMsgCanInteract msg;
    msg.ent_interact = ent_interact;
    msg.trigger_enter = trigger_enter;
    player->sendMsg(msg);
    return;
  }
  else if (ent_name == "materia") {
    // Hemos entrado en el trigger de la materia
    TMsgCanInteract msg;
    msg.ent_interact = ent_interact;
    msg.trigger_enter = trigger_enter;
    player->sendMsg(msg);
    return;
  }
  else if (ent_interact->get<TCompCuerda>().isValid() or ent_interact->get<TCompLight>().isValid()) {
    // Hemos entrado en el trigger de Columna | Cuerda | Luz
    TMsgCanInteract msg;
    msg.ent_interact = ent_interact;
    msg.trigger_enter = trigger_enter;
    player->sendMsg(msg);
  }
  else if (ent_interact->get<TCompUIInteraction>().isValid()) {
    TMsgCanInteract msg;
    msg.ent_interact = ent_interact;
    msg.trigger_enter = trigger_enter;
    player->sendMsg(msg);
  }
  else if ((!ent_interact->get<TCompUIInteraction>().isValid()) && (ent_interact->get<TCompAnimationObject>().isValid())) {
    TMsgActivateAniObject msg;
    msg.trigger_enter = trigger_enter;
    ent_interact->sendMsg(msg);
  }
  else if ((!ent_interact->get<TCompUIInteraction>().isValid()) && (ent_interact->get<TCompMovementObject>().isValid())) {
      TMsgActivateAniObject msg;
      msg.trigger_enter = trigger_enter;
      ent_interact->sendMsg(msg);
  }

}

void removeTriggerByEntity(string handle)
{
  CEntity* e = getEntityByName(handle);
  TCompCollider* col = e->get<TCompCollider>();
  const PxU32 numShapes =col->actor->getNbShapes();
  std::vector<PxShape*> shapes;
  shapes.resize(numShapes);
  col->actor->getShapes(&shapes[0], numShapes);
  for (PxU32 i = 0; i < numShapes; i++){
    PxShapeFlags flags = shapes[i]->getFlags();
    if (flags & PxShapeFlag::eTRIGGER_SHAPE) {
      
      shapes[i]->release();
      
    }
      
  }
  
}

bool isColumn(string handle)
{
  CEntity* e = getEntityByName(handle);
  return e->get<TCompColumn>().isValid();

}

bool isDeadZone(string handle)
{
    CEntity* e = getEntityByName(handle);
    if (!e)return false;
    TCompCollider* col = e->get<TCompCollider>();
    if (col) {
        return col->isDeadZone();
    }
}

void sendMessageDead()
{
  CEntity* player = getPlayerHandle();
  TMsgDeath msg;
  player->sendMsg(msg);
}

void sendMessageFallingDead()
{
CEntity* player = getPlayerHandle();
TMsgDeathFall msg;
player->sendMsg(msg);
}

void sendMessageEnd()
{
}

void fadeOut(float seconds)
{
  CEngine::get().getRender().fadeBlack(seconds);
}

void fadeIn(float seconds)
{
  CEngine::get().getRender().returnLight(seconds,1);
}

SoundEvent* playSound(string eventname)
{
  
  return CEngine::get().getSound().playEvent(eventname);
}

bool isAudioTrigger(string handle)
{
 
  return handle._Starts_with("audio");
}

void triggerAudio(string handle, string p)
{

  if (p.compare("Player") != 0)return;
  if(handle._Starts_with("audio_enemy")){
    CEngine::get().getSound().playBSO("guardiansBSO");
    CEntity* ai = getEntityByName("Boss");
    CAI_Controller* con = ai->get<CAI_Controller>();
    con->setGuardiansBSO(CEngine::get().getSound().getBSO());
    return;
  }
  string aux = handle;
  handle.erase(0, 6);
  
  if (handle._Starts_with("BSO")) {
    handle.erase(0, 4);
    CEngine::get().getSound().playBSO(handle);
  }
  else {
    CEngine::get().getSound().playEvent(handle);

    //destroy trigger
    CEntity* e = getEntityByName(aux);
    TCompCollider* c = e->get<TCompCollider>();
    c->destroyTriggers();
    //add trigger to player vector so it can be recreated when player dies
    CEntity* pl = getEntityByName("Player");
    TCompPlayer* player = pl->get<TCompPlayer>();
    player->addTrigger(aux);
  }
}

void playBSO(string eventname)
{
  CEngine::get().getSound().playBSO(eventname);
}

void sendContactWater(string name)
{

  CEntity* e = getEntityByName(name);
  TMsgWaterContact msg;
  e->sendMsg(msg);
}

bool isEnemyTrigger(string handle, string p)
{
  return p.compare("Player")==0 && handle._Starts_with("audio_enemy");
}

void playTemple()
{
  CEngine::get().getSound().playBSO("Temple");
  CEntity* ai = getEntityByName("Boss");
  CAI_Controller* con = ai->get<CAI_Controller>();
  con->setGuardiansBSO(nullptr);
}

void playCutscene(string name)
{
  //destroy trigger
  CEntity* e = getEntityByName(name);
  TCompCollider* c = e->get<TCompCollider>();
  c->destroyTriggers();
  //add trigger to player vector so it can be recreated when player dies
  CEntity* pl = getEntityByName("Player");
  TCompPlayer* player = pl->get<TCompPlayer>();
  player->addTrigger(name);
  name.erase(0,9);//remove cutscene_

  CEngine::get().getCutScenes().startCutscene(name);
}

bool isCutsceneTrigger(string handle, string p)
{
  return handle._Starts_with("cutscene") && p.compare("Player")==0;
}



void Exit()
{
  exit(0);
}

void removeEntityByName(string name)
{
  CEntity* e = getEntityByName(name);
  
  if(e) CHandle(e).destroy();
}


bool isCheckpoint(string name)
{
  CEntity* e = getEntityByName(name);

  return e->get<TCompCheckpoint>().isValid();
}

bool isPlayer(string name)
{
  CEntity* e = getEntityByName(name);
  if (!e)return false;
  return e->get<TCompPlayer>().isValid();
}

bool isWater(string name)
{
  CEntity* e = getEntityByName(name);
  TCompTags* tag = e->get<TCompTags>();

  if (!tag)return false;
  
  return tag->hasTag("Water");
}

void teleportPlayer(float x, float y, float z)
{
  VEC3 target = VEC3(x, y, z);
  VEC3 lookAt = VEC3(x + 1, y, z + 1);
  CEntity* player = getPlayerHandle();
  TCompCollider* col = player->get<TCompCollider>();
  TCompTransform* trans = player->get<TCompTransform>();
  trans->setPosition(target);
 
  PxExtendedVec3 vec;
  PxExtended px = x;
  PxExtended py = y+0.1;
  PxExtended pz = z;
  vec = PxExtendedVec3(px, py, pz);

  
  col->controller->setPosition(vec);
}

VEC3 getLastCheckPoint()
{
  CEntity* player = getPlayerHandle();
  TCompPlayer* pl = player->get<TCompPlayer>();
  return pl->getCheckPointPos();
}

void tp(int n)
{
  string name = "checkpoint" + to_string(n);
  CEntity* c = getEntityByName(name);
  TCompCheckpoint* t = c->get<TCompCheckpoint>();
  VEC3 pos = t->getCheckPoint();
  teleportPlayer(pos.x, pos.y, pos.z);
}




