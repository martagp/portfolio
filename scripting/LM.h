#pragma once
#include <stdlib.h>
#include <conio.h>
#include <stdio.h>
#include "SLB/include/SLB/SLB.hpp"
#include <string>
#include "mcv_platform.h"
#include "entity/entity_parser.h"
#include "components/common/comp_transform.h"
#include "sound/soundEvent.h"


using namespace std;

struct ScriptTask
{
  string code;
  //double birth;
  float delay;
};

class LM
{
public:
    
  LM();
  void Boot();
  bool ExecScript(string);
  void ExecScriptDelayed(string, unsigned long);
  void ExecFile(string);
  void Recalc(float delta);
  void Bind();
   

  void HelloWorld();
  void dbg(string x);
  void motherlode();

  
};

//GETS
CHandle getPlayerHandle();
VEC3 getLocationByHandle(CHandle handle);
CHandle getHandleByName(string s);
TCompTransform* getTransform(CHandle handle);
void getCheckpoint(string name, string Player);

//metodos spawn
CHandle spawnEnemy(string enemy, VEC3 pos, VEC3 lookDirection); // so the enemy isn't looking at the wall or something
CHandle spawn(string obj, VEC3 pos);
CHandle UI(string obj);

// Generic interaction
void onInteractionTrigger(string entity_name, bool enter);
void onInteraction(string entity_name);

//column utils
void sendInteractionMessage(string handle); // old method
bool checkColumnInput();
void sendInteractionMsgToPlayer(string columnName, bool trigger_enter);
void removeTriggerByEntity(string handle);
bool isColumn(string handle);


//delete
void removeEntityByName(string name);

//Checks
bool isCheckpoint(string name);
bool isPlayer(string name);
bool isWater(string name);

//general utils
void teleportPlayer(float x, float y, float z);
VEC3 getLastCheckPoint();

//death and End
bool isDeadZone(string handle);
void sendMessageDead();
void sendMessageFallingDead();
void sendMessageEnd();
void Exit();
void fadeOut(float seconds);
void fadeIn(float seconds);


//sound system
SoundEvent* playSound(string eventname);
bool isAudioTrigger(string handle);
void triggerAudio(string handle);
void playBSO(string eventname);
void sendContactWater(string name);
