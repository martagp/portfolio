
#include "mcv_platform.h"

#include "module_scripting.h"



//
//void CModuleScripting::ExecLuaScript(string command)
//{
//  try {
//    s->doString(command);
//  }
//  catch (std::exception x) {
//    dbg("error %s\n", x.what());
//  }
//}
//
//void CModuleScripting::ExecLuaScriptDelayed(string command, float delay)
//{
//  ScriptTask comand = { command, delay, Time.current };
//  Scripts.push_back(comand);
//}


void CModuleScripting::start()
{
  LM.Boot();
  //Bind Functions
  //Bind();

  //Set the namespace
  LM.ExecScript("SLB.using(SLB)");

  //Get all the files in the directory
  std::vector<std::string> files = getFilesInFolder("data/scripts/");

  //Execute the boot scripts
  for (const std::string& file : files) {   
    LM.ExecFile(file);
  }
}


  void CModuleScripting::stop()
  {
  }

  void CModuleScripting::update(float delta)
{
  LM.Recalc(delta);
}

  

void CModuleScripting::ConsoleCommand(char* comand)
{
  execCommand(comand);
}

void CModuleScripting::execCvar(std::string& script) {

    // Only backslash commands fetched.
    if (script.find("/") != 0)
        return;

    // Little bit of dirty tricks to achieve same results with different string types.
    script.erase(0, 1);
    int index = (int)script.find_first_of(' ');
    script = index != -1 ? script.replace(script.find_first_of(' '), 1, "(") : script;
    index = (int)script.find_first_of(' ');
    script = index != -1 ? script.replace(script.find_first_of(' '), 1, ",") : script;
    script.append(")");
}
bool CModuleScripting::execCommand(string code)
{
   return LM.ExecScript(code);
}

void CModuleScripting::execDelayed(string command, float delay)
{
  LM.ExecScriptDelayed(command, delay);
}


void CModuleScripting::ThrowEvent(Event e, const std::string &event_name, float delay)
{
  switch (e) {
  case TRIGGER_ENTER:
    if (delay > 0) {//las funciones se nombran on_Trigger_Enter_nombreEntidadTrigger_player() o on_Trigger_Enter_nombreEntidadTrigger_enemy(nombreEntidadEnemigo)
        execDelayed("on_Trigger_Enter(" + event_name + ")", delay);
    }
    else {
      execCommand("on_Trigger_Enter(" + event_name + ")");
    }
    break;
  case TRIGGER_EXIT:
    if (delay > 0) {//las funciones se nombran on_Trigger_Exit_nombreEntidadTrigger_player() o on_Trigger_Enter_nombreEntidadTrigger_enemy(nombreEntidadEnemigo)
        execDelayed("on_Trigger_Exit(" + event_name + ")", delay);
    }
    else {
      //dbg("Trigger Exit %s\n", event_name.c_str());
      execCommand("on_Trigger_Exit(" + event_name + ")");
    }
    break;
  case COLUMN_PUSH:
    execCommand("interact(" + event_name + ")");
    break;
  case INTERACT:
    execCommand("interact(" + event_name+ ")" );
	break;
  case CHECKPOINT:
    execCommand("saveCheckpoint( '" + event_name +"')");
    break;
  case INPUT:
    execCommand("on_input()");
    break;
  case TELEPORT_PLAYER:
    execCommand("teleportPlayer(" + event_name + ")");
    break;
  }
}


std::vector<std::string> CModuleScripting::getFilesInFolder(const std::string& folder) {
  const std::string extension = "lua";
  const std::string pattern = folder + "*";
  WIN32_FIND_DATA data;


  std::vector<std::string> found_files;

  HANDLE h = FindFirstFile(pattern.c_str(), &data);
  bool valid = (h != INVALID_HANDLE_VALUE);

 
  while (valid) {
    std::string filename = data.cFileName;
    if (filename != "." && filename != "..") {
      //Recursion
      if (data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
        std::vector<std::string> subf_files = getFilesInFolder(folder + filename + '/');
        found_files.insert(found_files.end(), subf_files.begin(), subf_files.end());
      } else if (hasExtension(filename, extension)) {
        string final_name = folder + filename;
        found_files.push_back(final_name);
      }
    }
    valid = FindNextFile(h, &data);
  }

  return found_files;
}

bool CModuleScripting::hasExtension(const std::string& filename, const std::string& extension)
{
  size_t point_pos = filename.find_last_of('.');
  return filename.substr(point_pos + 1) == extension;
}


