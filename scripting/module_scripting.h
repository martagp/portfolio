#pragma once

#include "modules/module.h"
#include "scripting/LM.h"


using namespace std;

class CModuleScripting final : public CModule {
private:
    LM LM;
    // void ExecLuaScript(string command);
    // void ExecLuaScriptDelayed(string command, float delay);
    std::vector<std::string> getFilesInFolder(const std::string& folder);
    bool hasExtension(const std::string& filename, const std::string& extension);

   
    /*struct ScriptTask{
        string code;
        unsigned long birth;
        unsigned long delay;
    };*/
    //std::list<ScriptTask> Scripts;
    // SLB::Manager* m;
    // SLB::Script* s;

public:
  enum Event {
    TRIGGER_ENTER,
    TRIGGER_EXIT,
    COLUMN_PUSH,
    INTERACT,
    CHECKPOINT,
    INPUT,
    TELEPORT_PLAYER,
  };

  CModuleScripting(const std::string& name) : CModule(name) { }
  void start() override;
  void stop() override;
  void update(float delta) override;

  void ConsoleCommand(char* comand);
  bool execCommand(string code);
  void execDelayed(string comand, float delay);

  void execCvar(std::string& script);

  void ThrowEvent(Event e, const std::string& param, float delay);

  //void Bind();

};