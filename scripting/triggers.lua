function interact(j)
	if  j == "materia" then
		playSound("collectMateria")
		--lm:ExecScriptDelayed("removeEntityByName(\"materia\")",100)
	end

	if(isColumn(tostring(j))) then
		removeEntityByName("dust_"..tostring(j))
		removeEntityByName("dust_rocks_"..tostring(j))
		--removeTriggerByEntity(tostring(j))
	end

	onInteraction(tostring(j))
end

function on_Trigger_Enter(j,p)
	if isPlayer(tostring(p)) then
		if isDeadZone(tostring(j)) then 
				sendMessageFallingDead()
		elseif j == "end" then
			fadeOut(1)
			lm:ExecScriptDelayed("Exit()",9000)
			lm:ExecScriptDelayed("UI(\"Final_Screen\")",1500)
		elseif isCheckpoint(tostring(j)) then
			getCheckpoint(tostring(j), tostring(p))
		
		else
			 if isAudioTrigger(tostring(j)) then
				triggerAudio(tostring(j))
			else  
				onInteractionTrigger(tostring(j), true)
			end
		end
	elseif isWater(tostring(j)) then
				sendContactWater(tostring(j))
	end
end

function on_Trigger_Exit(j,p)
	
	if isPlayer(tostring(p)) and not isPlayer(tostring(j)) then
		onInteractionTrigger(tostring(j), false)
	end
end