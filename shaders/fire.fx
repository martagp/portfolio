#include "common.h"
#include "pbr_functions.inc"
#include "pbr_gbuffer.inc"
#include "noise_functions.inc"

// FLAME MASK SHAPING
#define FLAME_SIZE 2.2
#define FLAME_WIDTH 1.3
#define DISPLACEMENT_STRENGTH 0.3
#define DISPLACEMENT_FREQUENCY 5.0
#define DISPLACEMENT_EXPONENT 1.5
#define DISPLACEMENT_SPEED 5.0
#define TEAR_EXPONENT 0.7
#define BASE_SHARPNESS 4.0

// NOISE
#define NOISE_SCALE 3.0
#define NOISE_SPEED -4.7
#define NOISE_GAIN 0.5
#define NOISE_MULT 0.35

// FLAME BLENDING
#define FALLOFF_MIN 0.2
#define FALLOFF_MAX 1.3
#define FALLOFF_EXPONENT 0.9

// COLOR
#define BACKGROUND_MIN 0.0
#define BACKGROUND_MAX 0.15
#define RIM_EXPONENT 2.0


// Information passed from VS gbuffer of a normal mesh to the PS
struct VOUT
{
  float4 Pos : SV_POSITION;
  float2 Uv : TEXCOORD0;
  float3 WorldPos : TEXCOORD2;
  float3 WorldNormal : TEXCOORD3;
  float4 WorldTan : TEXCOORD4;
  float3 V : POSITION;
};

//----------------------------------------------------------
VOUT VS(VtxPosNUvT input)
{
  float4x4 world = ObjWorld;

  VOUT output = (VOUT)0;

  float3 origin = world[3].xyz;
  float3 V = normalize(origin - CameraPos.xyz);
  float3 right = CameraLeft;   //normalize(cross(V, float3(0,1,0)));
  float3 up = float3(0, 1, 0);//normalize(cross(right, V));

  //float3 p = float3(origin.x, input.Uv.y, origin.z)*GlobalWorldTime;right *= 1+sin(noise(p));    
  output.Pos = float4(origin, 1);
  output.Pos.xyz += (input.Uv.x - .5) * right + input.Uv.y * up;
  output.Pos = mul(output.Pos, CameraViewProjection);
  output.WorldNormal = mul(input.N, (float3x3)world);

  output.Uv = input.Uv;

  return output;
}


float4 PS(VOUT input) : SV_Target
{
    float2 uv = input.Uv;
    float3    BACKGROUND_COLOR_MIN = float3(1, 0.0, 0.);
    float3   BACKGROUND_COLOR_MAX = float3(1.0, 0.3, 0.0);
    float3   RIM_COLOR = float3(1.0, 0.9, 0.0);

    if (projected) {

        BACKGROUND_COLOR_MIN = float3(0.2,0.6,0.8);
        BACKGROUND_COLOR_MAX = float3(0.3, 0.9, 0.8);
        RIM_COLOR = float3(0.35, 1,1);

    }
    // shape our base flame mask.
    // first we squish a circle and displace it, then we turn it into a teardrop shape
    float2 p = (uv - .5);
           p *= FLAME_SIZE;
           p.x *= FLAME_WIDTH;
    float time = sin(noise(ObjWorld[3].xyz)) * .5 + .5 + GlobalWorldTime;
    float dis_speed = DISPLACEMENT_SPEED * (sin(noise(ObjWorld[3].xyz)) * .5 + .5);
    float  flameDisplacement = max(0.0, sin(time * DISPLACEMENT_SPEED + (p.y * dis_speed)) * DISPLACEMENT_STRENGTH * pow(abs(uv.y - 0.1), DISPLACEMENT_EXPONENT));
    p.x += flameDisplacement;
    p.x += p.x / pow(abs(1.0 - p.y), TEAR_EXPONENT); // teardrop shaping

    // next we create our base flame mask, it looks a bit like a spooky ghost
    float gradient = length(p);
    float base = 1.0 - pow(gradient, BASE_SHARPNESS);

    // next we create our noise mask, which we will use to create the flickering part of the flame
    float speed = (sin(noise(ObjWorld[3].xyz)) * .5 + .5) * NOISE_SPEED * time;
    float up0 = snoise((uv * NOISE_SCALE) + float2(0.0, speed)) * NOISE_MULT + NOISE_GAIN;
    float up1 = 0.5 + snoise((uv * NOISE_SCALE) + float2(0.0,speed)) * NOISE_MULT + NOISE_GAIN;

    // define a gradient that we can use to make the flame fall off at the top and apply it to BOTH the flame mask and the noise together
    float falloff = smoothstep(FALLOFF_MIN, FALLOFF_MAX, pow(abs(uv.y), FALLOFF_EXPONENT));
    float flame = (base * up0 * up1);
          flame = clamp(flame - falloff, -0.0, 1.0); // we have a flame!


    float background = smoothstep(BACKGROUND_MIN, BACKGROUND_MAX, flame);
    float3 color = lerp(BACKGROUND_COLOR_MIN, BACKGROUND_COLOR_MAX, uv.y) * background;
    float s1 = step(0.24,flame);
    float s2 = step(0.7,flame);
    float3 dark = lerp(float3(0.0,0.0,0.0), float3(1.0, 0.4, 0.0),  s1);
    float3 light = lerp(dark, float3(1.0, 0.8, 0.0),  s2);

    if (projected) {
        dark = lerp(float3(0.0,0,0),float3(0.6,0.7,0.7),s1);
        light = lerp(dark, float3(1,1,1),s2);
            color = lerp(color, RIM_COLOR, light);
        float4 lastColor = float4(color * color * color,color.x * 6);
        if (lastColor.w < 0.1)discard;
        return lastColor;
    }

    color = lerp(color, RIM_COLOR, light);
    float4 lastColor = float4(color,color.x);
    if (lastColor.w < 0.1)discard;
    return lastColor;
}
