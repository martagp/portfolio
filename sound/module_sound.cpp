#include "mcv_platform.h"
#include "module_sound.h"
#include "engine.h"
#include "input/input_module.h"
#include "components/common/comp_tags.h"
#include "components/common/comp_transform.h"
#include "modules/module_camera_mixer.h"
#include <windows\application.h>

#pragma comment(lib, "fmodL_vc.lib")
#pragma comment(lib, "fmodstudioL_vc.lib")

using namespace input;
class SoundEvent;
unsigned int CModuleSound::eventID = 0;
void CModuleSound::start()
{
	result = Studio::System::create(&system);

	result = system->initialize(1024, FMOD_STUDIO_INIT_NORMAL, FMOD_INIT_NORMAL, extraDriverData);
	// Deshabilitamos el log de FMOD
	FMOD::Debug_Initialize(FMOD_DEBUG_LEVEL_NONE);

	// Deshabilitamos el log de FMOD
	FMOD::Debug_Initialize(FMOD_DEBUG_LEVEL_NONE);

	// arranco los Bancos Basicos
	result = system->loadBankFile("../Bin/data/sounds/banks/Master.bank", FMOD_STUDIO_LOAD_BANK_NORMAL, &masterBank);
	result = system->loadBankFile("../Bin/data/sounds/banks/Master.strings.bank", FMOD_STUDIO_LOAD_BANK_NORMAL, &stringsBank);

	// un par de bancos de ejemplo    
	result = system->loadBankFile("../Bin/data/sounds/banks/sinnBank.bank", FMOD_STUDIO_LOAD_BANK_NORMAL, &sinnBank);

	myBanks.emplace("Master", masterBank);
	myBanks.emplace("MasterString", stringsBank);
	myBanks.emplace("SINN", sinnBank);

	//recorro todos los banco y cargo los eventos
	for (auto& b : myBanks) {
		Studio::Bank* banquito = b.second;
		loadEvents(banquito);
	}
		
}

void CModuleSound::stop()
{
	cleanAllSound();

	sinnBank->unload();
	stringsBank->unload();
	masterBank->unload();

	system->release();
}

void CModuleSound::update(float elapsed)
{
	
	
	 /* Set listener according to active camera */
	CHandle candidate_h_listener = getEntityByName("Player");
	if (candidate_h_listener.isValid()) {
		setListener(candidate_h_listener);
	}

	/* Look for finished event instances */
	std::vector<unsigned int> done;
	for (auto& iter : myEventInstances) {
		FMOD::Studio::EventInstance* e = iter.second;
		FMOD_STUDIO_PLAYBACK_STATE state;
		e->getPlaybackState(&state);
		if (state == FMOD_STUDIO_PLAYBACK_STOPPED) {
			e->release();
			done.emplace_back(iter.first);
		}
	}

	for (auto id : done) {
		myEventInstances.erase(id);
	}
	if(effectsVolume != current_effects_volume)	
		updateVolumeToEffects();
	if (BSOVolume != current_bso_volume)
		updateVolumeToBSO();
	current_effects_volume = effectsVolume;
	current_bso_volume = BSOVolume;
	//esto siempre tiene que hacerlo
	system->update();
}

void CModuleSound::renderDebug()
{
	if (CApplication::get().ImGui) {
		if (ImGui::TreeNode("Sound")) {
			if (ImGui::TreeNode("Banks")) {
				for (auto bank : myBanks) {
					char path[512];
					bank.second->getPath(path, 512, nullptr);
					ImGui::Text(path);
				}
				ImGui::TreePop();
			}

			if (ImGui::TreeNode("Event Descriptions")) {
				int index = 0;
				for (auto ed : myEvents) {
					char path[512];
					ed.second->getPath(path, 512, nullptr);
					ImGui::Text(path);
					ImGui::SameLine();
					if (ImGui::Button(("Play " + std::to_string(index)).c_str())) {
						playEvent(path);
					}
					index++;
				}
				ImGui::TreePop();
			}

			if (ImGui::TreeNode("Event instances")) {
				int index = 0;
				for (auto ei : myEventInstances) {
					char path[512];
					FMOD::Studio::EventDescription* mydes;
					ei.second->getDescription(&mydes);
					mydes->getPath(path, 512, nullptr);
					if (ImGui::TreeNode(path)) {
						ImGui::SameLine();
						ImGui::Text(" - %s", getPlayingState(ei.second).c_str());
						ImGui::SameLine();
						SoundEvent* test;
						test = new SoundEvent(ei.first);
						ei.second->setUserData(&test);
						if (ImGui::Button(("Stop " + std::to_string(index)).c_str())) {

							test->stop();

						}
						bool changed = false;
						int numParameters = 0;
						for (auto ed : myEvents) {
							char eventpath[512];
							ed.second->getPath(eventpath, 512, nullptr);
							if (strcmp(path, eventpath) == 0) {
								ed.second->getParameterDescriptionCount(&numParameters);
								for (int i = 0; i < numParameters; i++) {
									FMOD_STUDIO_PARAMETER_DESCRIPTION* desc = new FMOD_STUDIO_PARAMETER_DESCRIPTION;
									result = ed.second->getParameterDescriptionByIndex(i, desc);
									float value = test->getParameter(desc->name);

									changed |= ImGui::DragFloat(desc->name, &value, 0.01f, desc->minimum, desc->maximum);

									if (changed) {
										test->setParameter(desc->name, value);
									}
								}
							}

						}
						ImGui::TreePop();
					}
					index++;
				}
				ImGui::TreePop();
			}
			ImGui::DragFloat("Effects Volume: ", &effectsVolume, 0.1f, 0.f, 10.f);
			ImGui::DragFloat("Music Volume: ", &BSOVolume, 0.1f, 0.f, 10.f);


			ImGui::TreePop();
		}
	}
}

void CModuleSound::loadEvents(Studio::Bank* bank)
{
	//load non-streaming data
	bank->loadSampleData();
	//get number of events in bank, so I can load them if there are any
	int nEvents = 0;
	bank->getEventCount(&nEvents);

	if (nEvents > 0) {
		// Get list of event descriptions in this bank
		std::vector<Studio::EventDescription*> events(nEvents);
		bank->getEventList(events.data(), nEvents, &nEvents);
		char name[512];
		for (int i = 0; i < nEvents; i++) {
			Studio::EventDescription* event = events[i];
			//we save the path of the event
			event->getPath(name, 512, nullptr);
		
			myEvents.emplace(name, event);
		}
	}
}

void CModuleSound::forceStopEvent(string event)
{
	string eventName = "event:/" + event;
	for (auto& iter : myEventInstances) {
		char path[512];
		FMOD::Studio::EventDescription* mydes;
		iter.second->getDescription(&mydes);
		mydes->getPath(path, 512, nullptr);
		if (eventName.compare(path) == 0) {
			iter.second->stop(FMOD_STUDIO_STOP_MODE::FMOD_STUDIO_STOP_IMMEDIATE);
			if (!current_bso)return;
			if (current_bso->getName().compare(event) == 0) {
				current_bso->stop();
				current_bso = nullptr;
			}
			return;
		}
		
	}
}

void CModuleSound::cleanAllSound()
{
	for (auto& desc : myBanks) {
		desc.second->unloadSampleData();
		desc.second->unload();
	}
	myBanks.clear();
	myEvents.clear();
}

void CModuleSound::updateVolumeToEffects()
{
	for (auto i : myEventInstances) {
		char path[512];
		FMOD::Studio::EventDescription* mydes;
		i.second->getDescription(&mydes);
		mydes->getPath(path, 512, nullptr);
		
		std::string name = path;
		name.erase(0, 7);
		float v = 0;
		float fv = 0;
		i.second->getVolume(&v, &fv);
		if(!current_bso->getName().compare(name)==0)
		
			i.second->setVolume(effectsVolume*fv);

	}
}

void CModuleSound::updateVolumeToBSO()
{
	for (auto i : myEventInstances) {
		char path[512];
		FMOD::Studio::EventDescription* mydes;
		i.second->getDescription(&mydes);
		mydes->getPath(path, 512, nullptr);

		std::string name = path;
		name.erase(0, 7);
		float v = 0;
		float fv = 0;
		i.second->getVolume(&v, &fv);
		if (current_bso->getName().compare(name) == 0)

			i.second->setVolume(BSOVolume * fv);

	}
}

SoundEvent* CModuleSound::playEvent(const std::string& name)
{
	
	std::string eventpath = "event:/" + name;
	unsigned int retID = 0;
	auto iter = myEvents.find(eventpath);
	SoundEvent* soundEvent;
	if (name == "") {
		soundEvent =new SoundEvent();
		return soundEvent;
	}
	if (iter != myEvents.end()) {

		/* Create instance of an event */
		FMOD::Studio::EventInstance* event = nullptr;
		iter->second->createInstance(&event);
		if (event) {

			/* Start the event instance */
			event->start();

			/* Get the next id and add it to map */
			eventID++;
			retID = eventID;
			myEventInstances.emplace(retID, event);
		}

		soundEvent = new SoundEvent(retID);
		soundEvent->setName(name);
		event->setUserData(&soundEvent);
	}
	else {
		soundEvent = new SoundEvent(retID);
	}
	return soundEvent;
}

void CModuleSound::playBSO(const std::string& name)
{
	if (name == "")return;
	if (current_bso) {
		if (current_bso->getName().compare(name)==0)return; //comprobar si la BSO que queremos poner ya esta reproduciendo
		else
			current_bso->stop(true);//si hay una bso reproduciendo que no sea la que queremos poner se parara para poder poner la nueva
			
	}
	current_bso = playEvent(name);
	changebso = false;
	newbso = "";
}



void CModuleSound::stopBSO()
{
	//dbg("paro \n");
	forceStopEvent(current_bso->getName());
	current_bso = nullptr;
}

FMOD::Studio::EventInstance* CModuleSound::getEventInstance(unsigned int id)
{
	FMOD::Studio::EventInstance* event = nullptr;
	auto iter = myEventInstances.find(id);
	if (iter != myEventInstances.end())
	{
		event = iter->second;
	}
	return event;
}

const std::string CModuleSound::getPlayingState(FMOD::Studio::EventInstance* ei)
{
	FMOD_STUDIO_PLAYBACK_STATE ei_state;
	ei->getPlaybackState(&ei_state);

	std::string name;
	switch (ei_state) {
	case FMOD_STUDIO_PLAYBACK_PLAYING:
		name = "Playing";
		break;
	case FMOD_STUDIO_PLAYBACK_SUSTAINING:
		name = "Sustaining";
		break;
	case FMOD_STUDIO_PLAYBACK_STOPPED:
		name = "Stopped";
		break;
	case FMOD_STUDIO_PLAYBACK_STARTING:
		name = "Starting";
		break;
	case FMOD_STUDIO_PLAYBACK_STOPPING:
		name = "Stopping";
		break;
	}
	return name;
}

CTransform CModuleSound::getVirtual3DAttributes(const CTransform& soundTransform, CHandle reference)
{
	if (h_listener.isValid()) {
		CEntity* e_listener = CEngine::get().getSound().getListener();
		TCompTags* listener_tags = e_listener->get<TCompTags>();

		if (listener_tags && listener_tags->hasTag(getID("camera_direction"))) {
			CEntity* e_reference = reference;
			TCompTransform* ppos = e_reference->get<TCompTransform>();
			TCompTransform* listenerPos = e_listener->get<TCompTransform>();

			float distance = VEC3::Distance(ppos->getPosition(), soundTransform.getPosition());
			VEC3 direction = (soundTransform.getPosition() - listenerPos->getPosition());
			direction.Normalize();
			VEC3 virtualPos = listenerPos->getPosition() + direction * distance;

			CTransform newTransform;
			newTransform.setPosition(virtualPos);
			newTransform.setRotation(soundTransform.getRotation());
			return newTransform;
		}
		else {
			return soundTransform;
		}
	}
	else {
		return soundTransform;
	}
}

void CModuleSound::setListener(CHandle h_listener)
{
	FMOD_3D_ATTRIBUTES attr;

	this->h_listener = h_listener;
	CEntity* e_listener = h_listener;
	TCompTransform* listener_pos = e_listener->get<TCompTransform>();
	attr.position = VEC3_TO_FMOD(listener_pos->getPosition());
	CEntity* cam = getEntityByName("mixed_camera");
	TCompTransform* cam_trans = cam->get<TCompTransform>();
	attr.forward = VEC3_TO_FMOD(-cam_trans->getFront());
	attr.up = VEC3_TO_FMOD(cam_trans->getUp());
	attr.velocity = VEC3_TO_FMOD(VEC3::Zero);
	system->setListenerAttributes(0, &attr);
}