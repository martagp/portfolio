#pragma once

#include "modules/module.h"
#include "entity/entity.h"
#include "sound/fmod/fmod_studio.hpp"
#include "sound/fmod/fmod.hpp"

#include "sound/soundEvent.h"

#define VEC3_TO_FMOD(VEC3) FMOD_VECTOR {VEC3.x, VEC3.y, VEC3.z};
#define FMOD_TO_VEC3(FMOD_VECTOR) VEC3(FMOD_VECTOR.x, FMOD_VECTOR.y, FMOD_VECTOR.z);

using namespace FMOD;
using namespace std;
class CModuleSound : public CModule {
protected:
  friend class SoundEvent;
private:
  //Parametros obligatorios de FMOD
  FMOD_RESULT result;
  bool first = true;
  void* extraDriverData = NULL;

  Studio::System* system = NULL; //System de Studio
  

  Studio::Bank* masterBank = NULL;		// ESTOS DOS SON OBLIGATORIOS, SON EL INDICE
  Studio::Bank* stringsBank = NULL;		// ESTOS DOS SON OBLIGATORIOS, SON EL INDICE

  Studio::Bank* sinnBank = NULL;		// estos son los bancos "reales" con datos
  
  //FMOD banks, events, eventinstances 
  std::unordered_map<std::string, FMOD::Studio::Bank*> myBanks;
  std::unordered_map<std::string, FMOD::Studio::EventDescription*> myEvents;
  std::unordered_map<unsigned int, FMOD::Studio::EventInstance*> myEventInstances;
  

  // ID for event instances  
  static unsigned int eventID;

  SoundEvent* current_bso = nullptr;
  float effectsVolume = 1.f;
  float BSOVolume = 1.f;
  float current_effects_volume=1.f;
  float current_bso_volume=1.f;
  CHandle h_listener;
  bool changebso = false;
  string newbso = "";
  //parameter value
  float parameterValue=0;
  void updateVolumeToEffects();
  void updateVolumeToBSO();
public:

  CModuleSound(const std::string& name) : CModule(name) {}
  void start() override;
  void stop() override;
  void update(float elapsed) override;
  void renderDebug();
  void loadEvents(Studio::Bank* bank);
  void forceStopEvent(string event);
  void cleanAllSound();

  

  SoundEvent* playEvent(const std::string& name);
  void playBSO(const std::string& name);
  void stopBSO();
  SoundEvent* getBSO() { return current_bso; };
  FMOD::Studio::EventInstance* getEventInstance(unsigned int id);
  const std::string getPlayingState(FMOD::Studio::EventInstance* ei);
	CTransform getVirtual3DAttributes(const CTransform& soundTransform, CHandle reference =  getEntityByName("Player"));

  void setListener(CHandle h_listener);
  void setBSOVolume(float v) { effectsVolume = v; };
  void setEffectsVolume(float v) { BSOVolume = v; };
  CHandle getListener() { return h_listener; };
};

