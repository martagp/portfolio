#include "mcv_platform.h"
#include "ui_change_textures_detection.h"
#include "ui_effect_wave.h"
#include "ui/widgets/ui_image.h"
#include "sinn/behavior/ai/ai_controller.h"
#include "engine.h"
#include "modules/module_camera_mixer.h"

namespace ui
{
  void CChangeTextureDetection::update(CWidget* widget, float elapsed)
  {
    CEntity* boss = getEntityByName("Boss");
    if (!boss) return;
    TImageParams* params = widget->getImageParams();
    if (!params) return;

    CAI_Controller* ai = boss->get<CAI_Controller>();
    //detection = ai->getValueDetection();
    //if (detection >= min[current] && detection <= max[current]) return;


    int state = (int)ai->getStateDetection();
    if (current == state) return;
    params->texture = textures[state];
    
    int last = current;
    current = state;

    if (state == 3 && last == 2) {
      CWidget* circle = widget->getSibilingById("onda_detectado");
      assert(circle);
      if (!circle) return;
      if (circle->getEffects().size() < 1) return;

      //CEffectWave* wave = dynamic_cast<CEffectWave*>(circle->getEffects()[0]);
      CEffectWave* wave = (CEffectWave*)(circle->getEffects()[0]);

      wave->start(circle);

      CModuleCameraMixer& mixer = CEngine::get().getCameraMixer();
      mixer.initShake(roll_max, time_max, wave_num);
    }
  }

  void CChangeTextureDetection::renderInMenu()
  {
    ImGui::DragInt("Estado", &current);
  }
}
