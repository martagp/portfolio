#pragma once

#include "ui/ui_effect.h"

//class CTexture;

namespace ui
{
  class CChangeTextureDetection : public CEffect
  {
  public:
    std::string_view getType() const override { return "Change Detection textures"; }
    void update(CWidget* widget, float elapsed) override;
    void renderInMenu();

    std::vector<const CTexture*> textures;
    std::vector<float> min;
    std::vector<float> max;

    int current;
    float detection;

    // Shake Camera
    float roll_max = deg2rad(5);
    float time_max = 0.5;
    int wave_num = 3;

  };
}
