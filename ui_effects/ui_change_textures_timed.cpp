#include "mcv_platform.h"
#include "ui_change_textures_timed.h"
#include "ui/widgets/ui_image.h"

namespace ui
{
    void CChangeTextureTimed::update(CWidget* widget, float elapsed)
    {
        _timer += elapsed;
        if (_timer > timers[_index]) {
            _timer = 0.f;
            _index = (_index + 1) % textures.size();
            TImageParams* params = widget->getImageParams();
            if (params)
            {
                params->texture = textures[_index];
            }
        }
    }
}
