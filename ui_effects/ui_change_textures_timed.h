#pragma once

#include "ui/ui_effect.h"

//class CTexture;

namespace ui
{
    class CChangeTextureTimed : public CEffect
    {
    public:
        std::string_view getType() const override { return "Change textures timed"; }
        void update(CWidget* widget, float elapsed) override;

    
        std::vector<const CTexture*> textures;
        std::vector<float> timers;
        int _index;
        float _timer;

       
    };
}
